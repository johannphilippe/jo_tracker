## jo_tracker

This project is a tracker software, designed for sound synthesis and composition. 
It's using IUP as a GUI, Terra and Lua as a main programming language, and Csound as audio engine. 
It's working on Linux and Windows (10). 

You'll need to know about Csound to use this software. 
https://csound.com/
And the manual :
http://www.csounds.com/manual/html/

You'll also need IUP to be installed. I recommand downloading the latest versions https://sourceforge.net/projects/iup/files/
But i also included the binaries in the lib folder, so that you only have to follow the README steps. 

# How to run it
---------------
The main code is called "jo_tracker.t". First open it with your favorite editor, and change the path if necessary. The variable "thispath" will get the current path. All you have to do
is to change the path so that lua and terra can find the libraries. Actually, if it doesn't find, it will tell you !
Then, in the bin folder, you'll find a terra standalone. It's this standalone that will read the jo_tracker.t file. 
On linux, open a terminal and type : ./bin/terra jo_tracker.t
You can also (both linux and window) add the terra binary to the environment, so you'll just have to type : 
terra jo_tracker.t


# How it works 
-------------
The menu Files : 
-Saveas : save the project as a .jo file (actually a lua file serialized)
-Save : saves in the last save file (Ctrl+s)
-Load : load a file (Ctrl+o)
-Load orchestra : loads a new "main" orchestra. For now, there is no way to change the master orc, so do the changes in the master orc file. 
-Export .CSD : exports the full project as a .csd csound file. It will contain the main orchestra, the master orchestra, the score and gen functions, and the -odac option. 
-Render stereo : renders a stereo .wav (no realtime processing) file of the project. 

The menu Actions : 
-actions to clear all, the sequence ... etc
-action to copy one seq to another seq location. 

The menu parameters : experimental, only resizing standard hidden tracks. 


The main window contains three tabs. The first tab is used to write the csound score, tempo score, and to play/stop performance. 
On the top left : 
-Play : to play realtime (shortcut spacebar)
-Record : to record realtime
-Stop : to stop realtime. (shortcut escape)
-two viewmeters for displaying amplitude
-One volume slider

On the top, some number boxes : 
-seqnbr : the current sequence. Just know that the first one is played, then the second starts etc... 
-tracknbr : the number of tracks.
-lignbr : the number of steps for the current sequence. 
-loop : the number of repetitions of the current sequence before the next one starts
-grid : shows a grid every x lines. It's just graphical. 

On the left area there is a track opened. You can add some new ones if you want, by incrementing the tracknbr value. 
In those tracks,you can type some score statements. First, always fill the first column field (otherwise, everything is deleted in the line). Then the tracker generates
p2 and p3 fields, initialised at 0 and 1. It's programmed to calculate automatically the p3 field. It will find the next occurrence of the same instance (of the instrument) or 
the next occurence of the negative same instance (-1.3), and do the math for you.
Then you can type any pfields you like, resizing the matrix etc... 
You can also write expressions (not for p1 p2 p3). 
Remember that a tracker reads time sequentially, from the top to the bottom. 



On the right area you'll find another matrix, defining tempo for the current sequence. By default, it's 60, like one step per second. If you want to initialize different, type on the 
first column of the first row. Then to make it evolve two possibilities : 
-Change it progressively by writing on the left column
-Chang it directly by writing on the left the "from" tempo, and on the right the "to" tempo. 

Second tab : GEN functions
The first numberbox shows the number of the current function displayed or edited. 
Here you'll find three main components :
-Draw a plot. It's designed to work with csound GEN 16. The four numbers on the top are used to change ymin,ymax,ystep,xstep. you can add points and move them with only clicking.
You can remove points with alt+click on the point. You can fix point on the closest gridstep with double click on the point. And you can curve a segment with alt+click on the segment 
and move up/down. 
-Import soundfile (bottom) : you just have to browse it. Know that the waveform display is still experimental. 
-Again, a matrix, (on the right) used to generate two kind of GEN : GEN02, a simple array of numbers, and GEN09 to write waveforms. 

Remember to store a function when you described it. 

The third tab is used for my personnal experiments, it's useless. 


#Shortcuts
-----------
On every matrix, i added 9 buffers able to contain a whole line of the matrix. To store a line, simply do Ctrl+1 (or any other number between 0-9 on keypad). Then,
you'll be able to paste it doing Alt+1 (or the correspondant number). 


# Sound Engine
--------------
The project uses csound syntax. But, for practical reasons, i decided to follow some conventions : 
-Separate two pieces of orchestra : the main, where sr, ksmps, nchnls etc are defined (and the instruments used), and the master where the output is defined. In the main, the global
audio are defined (here gaL and gaR). And in the master, they are redirected to the hardware output. 
So, for example, you could totally work with 4 channels, defining two more audio globals (gaRL gaRR for example) in the main orchestra, and affect them to outputs in the master orchestra.
Also, you'll find two master orchestra : one is designe for realtime playback with the software, the other one is used to render audio with realtime disabled. 

On the main orchestra, when you define an instrument, you can also add an info comment line, to have the pfields utility reported in the tracker. 
You'll find some examples in /csound/orc/mainOrc.orc

example : 
instr 1
;info1 1_oscil 2_date 3_dur 4_amp 5_cps
ao oscili p4,p5
gaL=gaL+ao
gaR=gaR+ao
endin

Also, in almost all instruments, i use a technique so that i can pass a GEN function OR a static value as a pfield. If i write a value >=0 then it takes the value
And if i write a value <0 then it takes the abs(value) function. 
Example : 

instr 1
;info1 1_oscil 2_date 3_dur 4_amp 5_cps
aidx linseg 0,abs(p3),1

if p5>=0 then
acps=p5
elseif p5<0 then
acps tablei aidx,abs(p5),1
endif
ao oscili p4,acps
gaL=gaL+ao
gaR=gaR+ao
endin 


An additionnal feature in the score writing (main tab, in the tracks) is that you can type  : scalefunc(from,min,max) where from is a GEN16 function (drawed in the second tab) 
and min the desired min value, and max the desired max. And this will make a copy of the function, resize it, and finally pass -(rescalesfuncnbr) (like -900 for example)
as the pfield. It must only be used with the upper mechanism in the sound engine. 




#What it contains, note for programmers : 
-----------------------------------------
This program is wrote using Lua and Terra language. Lua is a fast scripting and programming language (high level) while terra is it's low-level counterpart. 
http://terralang.org/

The jo_tracker.t file only describes this software. In the /share/lua and /share/terra, you'll find some files i wrote for general use, and some libraries i found online. 
share/terra : 
-miup_object.t : an sort of class with all sort of graphical objects
-sndfile.t : use the libsndfile C library inside terra for reading soundfile informations
-cs_api.t : use the csound C api in Terra
... the rest is just some tests

share/lua : 
-serpent : a library used to serialize lua tables - https://github.com/pkulchenko/serpent
-persistence : idem, not used in the project anymore
-luaxp : a lua library to evaluate lua expressions from strings : https://github.com/toggledbits/luaxp
-miup_utilities.lua : something i wrote for the everyday needs (table references,fifo buffers etc...)
-wav.lua : not used anymore, it's designed to read .wav files and get sample informations. 





#Licences

Copyright © 1994-2018 Tecgraf/PUC-Rio.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
