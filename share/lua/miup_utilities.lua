---------- MIUP utilities
local mutil={}

-- Useful if the item is a number box, and you want to pass the item instead of the value
--Because when passing the nbx.val from client, it will pass the value at init time. 
--So inside the miup_object, this function is used to get the current value, so the client just
--needs to pass the item as argument in general.
function mutil:getval(item)
local value
if type(item)~="table" then
	value=item
elseif type(item)=="table" then
	if item.val then
		value=item.val
	else
		value=item
	end
end

return value

end



function mutil:getidx(item,idx)
local value=item["idx"]
return value
end


--TODO
-- A function that compiles a hole table like {table,"value","3} to table.vvalue[3]
function mutil:get_table(tab)
--print("get_table",#tab)
local me=tab[1]
if #tab>1 then
	for i=2,#tab do
print(i)
		me=me[mutil:getval(tab[i])]
	end
end
return me

end

--For store function useful because it  returns the first argument "me" and the table with the rest
function mutil:get_store_vals(tab)

local me=tab[1]
local tb={}
if #tab>1 then
	for i=2,#tab do
		tb[i-1]=tab[i]
	end
end
return me,tb

end



function mutil:copy_ref(tab)

local tb={}
for k,v in pairs(tab) do
		tb[k]=v
end
return tb
end






------------------- HOMEMADE CIRCULAR BUFFER

function mutil:createbuffer(sz,init)
local buf={}
buf.init=init
buf.size=sz
buf.widx=1
buf.ridx_lifo=sz
buf.ridx_fifo=1
buf.wrapidx=0
if init then 
	for z=1,sz do
		buf[z]=init
	end
end

return buf


end


function mutil:writebuffer(buf,val)
        if buf.widx>buf.size then buf.widx=1 end
        buf[buf.widx]=val
        buf.widx=buf.widx+1

end


function mutil:readbuffer(buf,mode)
local val

        if mode=="FIFO" then
                if buf.ridx_fifo>buf.size then buf.ridx_fifo=1 end
                val=buf[buf.ridx_fifo]
                buf.ridx_fifo=buf.ridx_fifo+1
        elseif mode=="LIFO" then
                if buf.ridx_lifo<1 then 
                        buf.ridx_lifo=buf.size
                end

                val=buf[buf.ridx_lifo]
                buf.ridx_lifo=buf.ridx_lifo-1

        end


        return val
end






function mutil:clearbuffer(buf)

buf.widx=1
buf.ridx_lifo=buf.size
buf.ridx_fifo=1
buf.wrapidx=0
        for iter=1,buf.size do
                buf[iter]=buf.init
        end

return buf

end





return mutil
