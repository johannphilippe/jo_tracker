
local path_call=io.popen("pwd","r")
local thispath=path_call:read("*all")
path_call:close()
thispath=string.gsub(thispath,string.char(10),"")
package.cpath=package.cpath .. ";/usr/lib/lua/5.1/?.so"
package.path=package.path .. ";" .. thispath .. "/share/lua/?.lua"
package.terrapath=package.terrapath .. ";" .. thispath .. "/share/terra/?.t"

--terralib.includepath=terralib.includepath .. ";/usr/local/include/csound"

terralib.linklibrary("libsndfile.so")
local sndf=terralib.includec("sndfile.h")
local std=terralib.includec("stdlib.h")
local c=terralib.includec("stdio.h")




local sndLua={}


sndLua.data={}




function sndLua.setData(index,channel,value)

	if sndLua.data[index]==nil then sndLua.data[index]={} end
	sndLua.data[index][channel]=value


end


function sndLua.resetFile()
	sndLua.data={}
end



function sndLua.setInfo(channels,format,seekable,frames)

	sndLua.data.channels=channels
	sndLua.data.format=format
	sndLua.data.seekable=seekable
	sndLua.data.frames=frames

end





function sndLua.getFile()
return sndLua.data
end






sndLua.resetFileFromTerra=terralib.cast({} -> {},sndLua.resetFile)
sndLua.setInfoFromTerra=terralib.cast({int,int,int,int}->{},sndLua.setInfo)
sndLua.setDataFromTerra=terralib.cast({int,int,float} -> {},sndLua.setData)




terra sndLua.sndfile_read(fpath:&int8,peakpath:&int8)

	sndLua.resetFileFromTerra()

	var sfinfo:&sndf.SF_INFO = [&sndf.SF_INFO](std.malloc(sizeof(sndf.SF_INFO)))
	sfinfo.format=0
	
	var readfile:&sndf.SNDFILE =sndf.sf_open(fpath,sndf.SFM_READ,sfinfo)
	
	var BLOCK_SIZE:sndf.sf_count_t=512
	var nbr:int=sfinfo.channels*BLOCK_SIZE
	var buf:&float=[&float](std.malloc(nbr*sizeof(float)))



	var readcount:sndf.sf_count_t
	var k:int
	var m:int
	var totalcount:int=0


	var peakFile:&c.FILE=c.fopen(peakpath,"w")
	c.fprintf(peakFile,"#PEAKFILE#	DATA\n")
	--c.fprintf(peakFile,"local sf={}\n")
	--c.fprintf(peakFile,"1;\n")


	--c.fprintf(peakFile,"local sf={}")
	--c.fprintf(peakFile,"local channels=%d\n",sfinfo.channels)
	--c.fprintf(peakFile,"local format=%d\n",sfinfo.format)
	--c.fprintf(peakFile,"local seekable=%d\n",sfinfo.seekable)
	--c.fprintf(peakFile,"local frames=%d\n",sfinfo.frames)


	sndLua.setInfoFromTerra(sfinfo.channels,sfinfo.format,sfinfo.seekable,sfinfo.frames)

--readcount=sndf.sf_read_raw(readfile,buf,nbr)


	var ncnt:int=0
	readcount=sndf.sf_readf_float(readfile,buf,BLOCK_SIZE)
	while readcount>0 do	
			for k=0,readcount-1,32 do

				totalcount=totalcount+1
				m=0
				--c.fprintf(peakFile,"sf={",totalcount)
				while m<sfinfo.channels do
					--c.fprintf(peakFile,"sf[%d][%d]=%12.10f\n",totalcount,m,buf[k*sfinfo.channels+m])
					if m==0 then
						c.fprintf(peakFile,"%d\t%12.10f\n",ncnt,buf[k*sfinfo.channels+m])
						ncnt=ncnt+1
					end
					if m<sfinfo.channels then
						--c.fprintf(peakFile,";")
					end
					--sndLua.setDataFromTerra(totalcount,m+,buf[k*sfinfo.channels+m])
					m=m+1
					
				end
--				c.fprintf(peakFile,"}\n")	

				c.fprintf(peakFile,"\n")	
			end
	readcount=sndf.sf_readf_float(readfile,buf,BLOCK_SIZE)
	end

	var res:int=sndf.sf_close(readfile)
	--c.fprintf(peakFile,"return sf\n")
	c.fclose(peakFile)
--	std.free(readfile)

end



function sndLua.getPeak(filepath,peakpath)
local test=io.open(peakpath,"r")
local flag
	if test==nil then
		sndLua.sndfile_read(filepath,peakpath)
		flag=false
	else
		flag=true	
		test:close()
	end

	return flag

end


return sndLua


