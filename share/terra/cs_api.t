------------------------------------------ TERRA FUNCTIONS --------------------------------------------
--------------------------------------------FOR CSOUND API ------------------------------------------
local ffi=require("ffi")

--terralib.linklibrary("libcsound64.so")


--useful only for compiling binary (so, something like useless)
terralib.includepath=terralib.includepath .. ";/usr/local/include/csound"

 c=terralib.includec("stdio.h")
 csnd=terralib.includec("csound.h")
 std=terralib.includec("stdlib.h")
 str=terralib.includec("string.h")

perf_stat=global(int)
mast=global(double,1)



struct graphic_cbk{
idx:&int8;
val:&double;
}




struct bufs{
mast:double;
bufM:&opaque;
mtx:&opaque;
mtx_stat:int;
gui_buf:&opaque;
gui_cbk:graphic_cbk;
gui_read:graphic_cbk;
}



struct U_Data {
result : int;
PERF_STATUS : int;
csound:&csnd.CSOUND_;
mastval:double;
mtx :&opaque;
flt:&double;
bufM:&opaque;
}




------------------------------------ REALTIME FUNCTIONS -------------------------------------------







-- BUF WRITE FOR MASTER SLIDER
--- BOTH WORK, or i call the lua function from lua (or directly set the value of mast with mast:set)
--or i call a terra function which work is to set the value
--So i could do it directly in the "connect" slider method


--[[
function buf_write(shit,mval)
mast:set(mval)
end
]]--

terra buf_write(dataptr:&opaque,mval:double)
var ud:&U_Data=[&U_Data](dataptr)
mast=mval
end











terra callback_sync(csound:&csnd.CSOUND_,chn:&int8,valptr:&opaque,chntype:&opaque)

	var dataptr=csnd.csoundGetHostData(csound)
        var hd:&bufs=[&bufs](dataptr)



	csnd.csoundLockMutex(hd.mtx)
	
		hd.gui_cbk.idx=chn
		hd.gui_cbk.val=[&double](valptr)
		csnd.csoundWriteCircularBuffer(csound,hd.gui_buf,&hd.gui_cbk,1)

	csnd.csoundUnlockMutex(hd.mtx)

end









terra performance(dataptr:&opaque) : uint64

        var ud:&U_Data=[&U_Data](dataptr)

	var dptr=csnd.csoundGetHostData(ud.csound)
        var hd:&bufs=[&bufs](dptr)
c.printf("GET USER DATA IN PERF THREAD\n")


	while csnd.csoundPerformKsmps(ud.csound)==0 and perf_stat==1 do
--		csnd.csoundLockMutex(ud.mtx)
		csnd.csoundWriteCircularBuffer(ud.csound,ud.bufM,&mast,1)
		csnd.csoundReadCircularBuffer(ud.csound,ud.bufM,ud.flt,1)
--		csnd.csoundUnlockMutex(ud.mtx)
	end
	
       -- csnd.csoundReset(ud.csound)
c.printf("CLEANUP AND DESTROY\n")

	perf_stat=0

	csnd.csoundCleanup(ud.csound)
	csnd.csoundDestroy(ud.csound)
	std.free(ud)
	std.free(hd)

end


terra cs_start_perf(orc:&int8,sco:&int8) : &bufs
	if perf_stat==0 then	
		c.printf("BEGINN START PERF\n")

        	var ud:&U_Data = [&U_Data](std.malloc(sizeof(U_Data)))
        	var hd:&bufs = [&bufs](std.malloc(sizeof(bufs)))

		ud.result=csnd.csoundInitialize(0)
		c.printf("INITIALIZER\n")
		ud.csound=csnd.csoundCreate(hd)

		c.printf("CSOUND CREATED\n")
		csnd.csoundSetOption(ud.csound, '--output=dac')
		if ffi.os=="Linux" then
		csnd.csoundSetOption(ud.csound, '-+rtaudio=jack')
		csnd.csoundSetOption(ud.csound, '-Qhw:2,0,0')
		end
		--csnd.csoundSetOption(ud.csound, '-+rtmidi=jack')
		csnd.csoundSetOption(ud.csound, '-iadc')
		csnd.csoundSetOption(ud.csound, '--nodisplays')
        	csnd.csoundSetOption(ud.csound, '-B4096')
                --csnd.csoundSetOption(ud.csound, '-+rtaudio=ASIO')
                csnd.csoundSetOption(ud.csound, '--realtime')
                csnd.csoundSetOption(ud.csound,'-b1024')
        	--csnd.csoundSetOption(ud.csound, '--num-threads=2') 
		csnd.csoundSetOption(ud.csound,'-j3')
		hd.gui_buf=csnd.csoundCreateCircularBuffer(ud.csound,256,sizeof(graphic_cbk))	
		ud.bufM=csnd.csoundCreateCircularBuffer(ud.csound,32,sizeof(double))	
        


		hd.mtx=csnd.csoundCreateMutex(1)
		--ud.mtx=csnd.csoundCreateMutex(1)
        
		c.printf("BUFFERS AND MUTEX OK %d\n",ud.result)
		if csnd.csoundCompileOrc(ud.csound,orc)==0 and csnd.csoundReadScore(ud.csound,sco)==0 then
--		ud.result=csnd.csoundCompileOrc(ud.csound,orc)
	c.printf("COMPILE %d \n",ud.result)
  --      	ud.result=csnd.csoundReadScore(ud.csound,sco)
	c.printf("READSCO %d \n",ud.result)

		csnd.csoundSetOutputChannelCallback(ud.csound,callback_sync)
        	csnd.csoundGetChannelPtr(ud.csound,&ud.flt, 'Master',csnd.CSOUND_INPUT_CHANNEL or csnd.CSOUND_CONTROL_CHANNEL)
        

		c.printf("CHANNELS AND CBKS OK\n")
	
		@ud.flt=mast
		perf_stat=1
--		if ud.result==0 then
			csnd.csoundStart(ud.csound)

		c.printf("START INSTANCE\n")
       			var ThreadID=csnd.csoundCreateThread(performance,ud)

		else
		end	

			return hd

--		end
	end
end



terra cs_stop()
        --var ud:&U_Data=[&U_Data](dataptr)
        perf_stat=0
        --return ud

end























------------------------------------ OFFLINE RENDERING ----------------------------------------------
----------------------------------------------------------------------------------------------------



terra render_thread(dataptr:&opaque) :uint64
        var ud:&U_Data=[&U_Data](dataptr)

	while csnd.csoundPerformBuffer(ud.csound)==0 do
	end
	csnd.csoundDestroy(ud.csound)
std.free(ud)

end


terra render(orc:&int8,sco:&int8,path:&int8,filetype:&int8)

--c.printf("%s %c %s %c %s %c",orc,10,sco,10,path)
        var ud:&U_Data = [&U_Data](std.malloc(sizeof(U_Data)))

	ud.result=csnd.csoundInitialize(0)
	ud.csound=csnd.csoundCreate(ud)
	csnd.csoundSetOption(ud.csound,filetype)
	csnd.csoundSetOption(ud.csound,path)
        --csnd.csoundSetOption(ud.csound, '-B1024')
        --csnd.csoundSetOption(ud.csound, '--num-threads=8') 
	--csnd.csoundSetOutput(ud.csound,path,filetype,"24BIT")


	ud.mtx=csnd.csoundCreateMutex(1)
        

	ud.result=csnd.csoundCompileOrc(ud.csound,orc)
        csnd.csoundReadScore(ud.csound,sco)


	csnd.csoundStart(ud.csound)
       	var ThreadID=csnd.csoundCreateThread(render_thread,ud)
--std.free(ud)
end






