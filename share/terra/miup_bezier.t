
-----------------------------------------------------------------------------------------------------------------------------------------
print("pre miup")
os.setlocale("en_US.utf8")

print("os.setlocale")

local iup=require("iuplua"),require("iupluacontrols"),require("iuplua_plot"),require("iupluaimglib")
local cd=require("cdlua")

local wav=require("wav")
local mutil=require("miup_utilities")
local csnd = require("cs_api")
local xp=require("luaxp")
local ffi=require("ffi")
local miup={}

print("miup required all")
------------- This function creates a new miup object - can be used to create any standard MIUP widget



function miup:new(o)
        o=o or {}
        setmetatable(o,self)
        self.__index=self
	--o.parent=self
	return o
        
end







function miup:slider(val,min,max,orientation,step)
	local oself=self
        self.sl=iup.val{value=val,min=min,max=max}
        if step then self.sl.step=step end
        if orientation then self.sl.orientation=orientation end
        self.lb=iup.label{title=tostring(self.sl.value)}
        self.val=val 

	function self.sl:valuechanged_cb()
		oself.lb.title=tostring(self.value)
		oself.val=tonumber(self.value)
		oself:slider_connect("postconnect")
	end

end


function miup.slider_getval(self)
	return self.val
end




function miup:slider_connect(pre_post)
if self[pre_post] and type(self[pre_post])=="table" then
       for k,v in pairs(self[pre_post]) do
                if v[1]=="val" then
                     

                elseif v[1]=="method" then


                elseif v[1]=="store" then

                elseif v[1]=="terra_connect" then

--		v[2].mastval=self.val
		buf_write(v[2],self.val)		

--		local idx=v[3]
--                local terra terval(dataptr : &opaque, val:double)
--                var ud:&U_Data=[&U_Data](dataptr)
--                ud.[idx]=val
--                end
--                terval(v[2],self.val)


                end



        end 
        
        


end
end
















function miup:multislider(sl_nbr,sl_init,min,max)
local oself=self
        self.multislider=iup.plot{
        AXS_YAUTOMIN="NO",
        AXS_YAUTOMAX="NO",
        AXS_YMIN=min,
        AXS_YMAX=max,
--        GRAPHICSMODE="NATIVE",
        HIGHLIGHTMODE="SAMPLE", --could be "BOTH" for curve and sample
        MENUCONTEXT="YES",
        READONLY="NO",
        SCREENTOLERENCE=5,
        MENUITEMPROPERTIES="YES",
        PLOT_CURRENT=0,
        EDITABLEVALUES="yes",
        AXS_XTICK="NO",
        AXS_YTICK="YES",
        BOX="YES",
        GRID="YES",
}
self.multislider:Begin(0)
self.multislider:End()
        
        self.multislider.DS_CURRENT=0
        self.multislider.DS_MODE="BAR"
--self.multislider:Insert(0,0,0,0.25)

--self.multislider:Insert(0,1,0.5,0.55)
        for aw=1,sl_nbr do
        self.multislider:Insert(0,aw,aw,sl_init)
        end
        self.multislider.redraw="yes"
local cnvx,cnvy





local ret,dsfound,sampfound,selstat
function self.multislider:plotbutton_cb(but,press,x,y,stat)
       if press==1 then
                
                sampfound=math.floor(x-0.5)
                dsfound=0
                cnvx,cnvy=self:Transform(x,y)
                --ret,dsfound,sampfound=self:FindSample(cnvx,cnvy)
                
                self:SetSampleSelection(0,sampfound,1)
                selstat=true
              --[[ 
               if ret==1 then
                       self:SetSampleSelection(dsfound,sampfound,1)
                       selstat=true
                end
]]--
        elseif press==0 and selstat==true then
                
                self:SetSampleSelection(0,sampfound,0)
               -- self:SetSampleSelection(dsfound,sampfound,0)
                selstat=false
        end




end


function self.multislider:plotmotion_cb(x,y,r)
       if dsfound and sampfound and selstat==true then 
                 
               local findx,findy=self:GetSample(dsfound,sampfound)
                self:SetSample(dsfound,sampfound,findx,y)
		self.redraw="yes"
        end

end


end










---------------------------------------------------------------------
--NUMBER BOX METHOD (increment and decrment with spin or up and low arrow  keys)
--------------------------------------------------------------------



function miup:nbx(init,min,max,decimal,step,spin)


	local oself=self        
	self.init=init
	self.val=init
	self.key_flag=true


	if decimal and decimal~=0 then 
	        step=step
	else
	        step=1
	end

	self.dec=decimal
	self.step=step
	if min then self.min=min end
	if max then self.max=max end
        self.txt=iup.text{value=init,readonly="yes",canfocus="yes"}


	if spin and spin~=0 then 
       --         local updn=require("arrows")

--                self.upspin=iup.button{image=load_image_sup()}
--                self.downspin=iup.button{image=load_image_sdown()}
	
                self.upspin=iup.button{image="IUP_ArrowUp"}
                self.downspin=iup.button{image="IUP_ArrowDown"}


                self.spinb=iup.vbox{self.upspin,self.downspin}
                self.upspin.size="15x8"
                self.downspin.size=self.upspin.size
                self.spinb.size=self.txt.size
                self.nbx=iup.hbox{self.txt,self.spinb,alignment="acenter"}


		local function spincb (l)
--		        print("SPIN --> ",l) 
			oself:nbx_connect("preconnect")
		
		        local tval=oself:nbx_spincbk(l)
		        oself.val=tval
		        oself:nbx_connect("postconnect")
			collectgarbage("collect")
		end


                function self.upspin:button_cb(button,pressed,x,y,stat)
                        local state=string.gsub(stat," ","")                        
                        if pressed==0 then
                                if state=="1" then
                                        spincb(1)
                                elseif state=="C1" then
                                        spincb(2)
                                elseif state=="S1" then
                                        spincb(10)
                                end
                        end
                end

                function self.downspin:button_cb(button,pressed,x,y,stat)
                        local state=string.gsub(stat," ","")                        
                        if pressed==0 then
                                if state=="1" then
                                        spincb(-1)
                                elseif state=="C1" then
                                        spincb(-2)
                                elseif state=="S1" then
                                        spincb(-10)
                                end
                        end
                end     

	else self.nbx=self.txt end

	function self.txt:k_any(l)
		if oself.key_flag==true then 
       			oself:nbx_connect("preconnect")
		end
	        local tval=oself:nbx_keycbk(l)     
	        oself.val=tval
		if oself.key_flag==true then
		        oself:nbx_connect("postconnect")
			oself:select()
		end
		collectgarbage("collect")

	end

end





-------------------- Old spin method - use the internal iup spinbox, with no antibound solution


function miup:old_nbx(init,min,max,decimal,step,spin)
	local oself=self        
	self.init=init
	self.val=init
	self.key_flag=true


	if decimal and decimal~=0 then 
	        step=step
	else
	        step=1
	end

	self.dec=decimal
	self.step=step
	if min then self.min=min end
	if max then self.max=max end
        self.txt=iup.text{value=init,readonly="yes",canfocus="yes"}

	if spin and spin~=0 then self.nbx=iup.spinbox{self.txt}





		function self.nbx:spin_cb(l)
		        print("SPIN --> ",l) 
			oself:nbx_connect("preconnect")
		
		        local tval=oself:nbx_spincbk(l)
		        oself.val=tval
		        oself:nbx_connect("postconnect")
			collectgarbage("collect")
		end



	else self.nbx=self.txt end

	function self.txt:k_any(l)
		if oself.key_flag==true then 
       			oself:nbx_connect("preconnect")
		end
	        local tval=oself:nbx_keycbk(l)     
	        oself.val=tval
		if oself.key_flag==true then
		        oself:nbx_connect("postconnect")
			oself:select()
		end
		collectgarbage("collect")
	end



end 




function miup:select()
iup.SetFocus(self.txt)
end




--Number box increment and decrement methods (externalised from main method so it can be used outside)


-- The nbx_connect is used for numberbox (miup:nbx) - it allows it to be connected to some other values, and to change it in a particular trigger order
-- equivalent of the max msp "trigger" - there is a preconnect (executed before  the value of the number box is changed) and a postconnect (after the value has changed)
--
--val assigns the value of the number box to another  value (example the number of lines in a matrix)
--assign is used for recall - it gets vack a value from a data and changed the targeted number box value - and then executes the preconnect and postconnect functions
--of this number box. It uses the miup:assign method
--
--method is used to connect to any function using the format miup.something (not working with miup:something) with the following arguments : 
--self.method(self,{arguments})
--
--
--
function miup:nbx_connect(pre_post)
if self[pre_post] then
        local t=self[pre_post]
        for k,v in pairs(t) do
                if t[k][1]=="val" then
                        if t[k][3] then
                        t[k][2][t[k][3]]=self.val
                        else t[k][2]=self.val
                        end


--                elseif t[k][1]=="terraval" then
                        

                elseif t[k][1]=="assign" then
                        t[k][2][t[k][3]]=t[k][4]




                elseif t[k][1]=="method" then
                        if t[k][4]==nil then
                        t[k][2][t[k][3]](t[k][2],self)
                        elseif t[k][4]~=nil then
                        t[k][2][t[k][3]](t[k][2],t[k][4])
                        end

        
                elseif t[k][1]=="store" then
                        local valtostore
                        if type(t[k][3])=="table" then
                        valtostore=t[k][3].val
                        else
                        valtostore=t[k][3]
                        end
                        local idx={}
                        for k,v in pairs (t[k][4]) do
                                if type(v)=="table" then 
                                        idx[k]=v.val
                                else
                                        idx[k]=v
                                end
                        end
                        t[k][2]:store(valtostore,idx)


                       
                elseif t[k][1]=="storemat" then
                        t[k][2]:storemat(t[k][3])
 
                 end
        
        end
               

end


end




function miup.assign(self,tab)
local myself=tab[1]

	if tab[2] then        
	        for iter=2,#tab do
			--print("GETVAL",iter,getval(tab[iter]))
		        if type(tab[iter])=="string" then
        		        myself=myself[tab[iter]]
        		elseif type(myself[tab[iter].val])=="table" then
        		        myself=myself[tab[iter].val]
        		elseif type(tab[iter])=="number" then
        		        myself=myself[tab[iter]]
        		end
        	end
	end
        
	if myself and type(myself)=="number" then
                self.txt.value=myself
        else
                self.txt.value=self.init
        end
        
	self:nbx_connect("preconnect")
        
	local tval=tonumber(self.txt.value) 

        self.val=tval
        self:nbx_connect("postconnect")
        

end






--spin inc dec method
function miup:nbx_spincbk(l)
        local tval=self.txt.value+(l*self.step)

        if self.max and tval>self.max then tval=self.max 
        elseif self.min and tval<self.min then tval=self.min
        end
        if self.dec==0 then tval=math.floor(tval) end

        self.txt.value=tval
        return tval
end




--key inc dec method
function miup:nbx_keycbk(l) 
	local tval=self.txt.value
	if self.last==nil then self.last=0 end
	        if l==65361 or l==65363 then
	         self.less=1
	           if tval==nil then 
	                 tval=0 end
	        end

	        if l==65363 then 
			self.key_flag=true
			tval=tval+self.step
	        elseif l==65361 then 
			self.key_flag=true
			tval=tval-self.step
	        elseif l>=48 and l<=57 then
			self.key_flag=false
	                if self.last>=48 and self.last<=57 then
	                     tval=tonumber(string.format("%s%d",tostring(tval),string.char(l)))
	                elseif self.last==46 then
	                        tval=tonumber(string.format("%d.%d",tval,string.char(l)))
	                else 
	                     tval=tonumber(string.char(l))
	
	                end

	        elseif l==8 then tval=0
			self.key_flag=false
	                self.less=1
	        elseif l==45 then 
	                if self.last==8 then 
	                        self.less=-1
	                elseif self.last==0 then
        	                self.less=-1
        	        end
		elseif l==13 then
			self.key_flag=true
		--	self.assign(self,{self})

		end


	if not self.less then self.less=1 end

	tval=tonumber(tval)*self.less	
	if self.min and tval<self.min then tval=self.min
	elseif self.max and tval>self.max then tval=self.max
	end
	if self.dec==0 then tval=math.floor(tval) end
	self.txt.value=tval
	self.last=l
	return tval
end













------------------------------------Mix Matrix


function miup:stepseq()



end










-------------------------------------------------------------------------------------------------------------------------------------
                                                --IUP MATRIX USED FOR ARRAYS AND DATA EDITION AND MANAGEMENT
-------------------------------------------------------------------------------------------------------------------------------------




function miup:matrix(clnbr,lgnbr,storetab)

	if not self.storetab then self.storetab=mutil:copy_ref(storetab) end 

	if not self.copy_buffer then self.copy_buffer=miup:new() end


--self.storetab=storetab

	local oself=self
	local lin,col

-- This flag is used to avoid the enteritem to activate during the callback of  my seqnbr
	self.cb_flag=true


	self.initlin=lgnbr
	self.initcol=clnbr


	self.mat=iup.matrixex{numcol=clnbr,numlin=lgnbr,numcol_visible=10,numlin_visible=10,widthdef=50,
	resizematrix="YES",UNDOREDO="YES",MARKMODE="CELL",MARKMULTIPLE="YES",MARKAREA="NOT_CONTINUOUS",
	COPYKEEPSTRUCT="YES",LASTERROR="MARKEDCONSISTENCY",hidefocus="no"}
	--self.mat.HIDDENTEXTMARKS="YES"
	--self.mat["WIDTH0"]=10
	self.mat["FGCOLOR*:0"]="200 200 200"
	self.mat["FGCOLOR0:*"]="200 200 200"
	self.mat["FGCOLOR0:0"]="200 200 200"
	for idx=0,lgnbr do self.mat:setcell(idx,0,tostring(idx)) end

--self.cb_flag=true


	self.clearbut=iup.button{title="Clear",action=function()
	self.clearall(self)
	self:storemat(self.storetab)
	end}





function self.mat:redraw_mat()
	self.redraw="yes"
end


function self.mat:markedit_cb()
	self.redraw="yes"
end

function self.mat:enteritem_cb(l,c)
	lin,col=l,c
	if oself.cb_flag==true then
		local mk=string.format("MARK%d:%d",l,c)
		self[mk]=1
		self.redraw="yes"
		oself:mat_connect("preconnect",{lin,col})
	end
end

function self.mat:leaveitem_cb()
	local mk=string.format("MARK%d:%d",lin,col)
	self[mk]=0
	self.redraw="yes"

	local eval=self:getcell(lin,col)

	if type(eval)=="number" then
	
	elseif type(eval)=="string" then


	local result,message=xp.evaluate(eval)
		if result==nil then
			print("exp failed  ** ", message)
			self:setcell(lin,col,nil)
		else
			print(result,type(result))
			self:setcell(lin,col,result)
		end

	end
--if oself.cb_flag==true then
        oself:mat_connect("postconnect",{lin,col})
--end
end




local function bufferize(idx)
	local test=self.mat:getcell(lin,1) 
	if test~=nil then
	        if self.copy_buffer[idx] and self.copy_buffer[idx][1]~=nil then self.copy_buffer:mydestroy({idx}) end

local stab=mutil:get_table(self.storetab)
print("STABBBBB ",#stab[lin])
		for k,v in pairs(stab[lin]) do 
			local val=v
			print("VALUE ",val)
		        if val~=nil then
			        self.copy_buffer:store(val,{idx,k})
        		end
		end
	end
end


local function paste_buffer(idx)
if self.copy_buffer[idx] and self.copy_buffer[idx][1]~=nil then

        for a=1,#self.copy_buffer[idx] do
	        local val=self.copy_buffer[idx][a]
		local idx_st=self.storetab[1]
		local stab=mutil:copy_ref(storetab)
		table.remove(stab,1)
		table.insert(stab,lin)
		table.insert(stab,a)
	        if val and val~=nil then
		        self.mat:setcell(lin,a,val)
			idx_st:store(tonumber(val),stab)
        	else

        		self.mat:setcell(lin,a,nil)
			idx_st:store(nil,stab)

        	end
        end
--self:storemat(self.storetab)

	self.mat.redraw="yes"
--self:storemat(storetab)
end


end


function self.mat:k_any(l)
if l==8 then 
        self:setcell(lin,col,nil)
        self.redraw="yes"
	elseif l==536870961 then bufferize(1)
	elseif l==536870962 then bufferize(2)
	elseif l==536870963 then bufferize(3)
	elseif l==536870964 then bufferize(4)
	elseif l==536870965 then bufferize(5)
	elseif l==536870966 then bufferize(6)
	elseif l==536870967 then bufferize(7)
	elseif l==536870968 then bufferize(8)
	elseif l==536870969 then bufferize(9)
	
	elseif l==1073741873 then paste_buffer(1)
	elseif l==1073741874 then paste_buffer(2)
	elseif l==1073741875 then paste_buffer(3)
	elseif l==1073741876 then paste_buffer(4)
	elseif l==1073741877 then paste_buffer(5)
	elseif l==1073741878 then paste_buffer(6)
	elseif l==1073741879 then paste_buffer(7)
	elseif l==1073741880 then paste_buffer(8)
	elseif l==1073741881 then paste_buffer(9)

end
print(l)



end







end





function miup.mat_colorline(self,gridstep)


local step

if gridstep.val then
step=gridstep.val
elseif gridstep[1].val then
step=gridstep[1].val
end
local color

if step and step>0 then
        for thisline=1,tonumber(self.mat.numlin),step do 
                for fill=thisline,thisline+step-1 do
                        local theline=string.format("%s%d%s","BGCOLOR",fill,":*")

                       if fill==thisline then color="150 150 150" else color="254 254 254" end
        
                        self.mat[theline]=color
                end 
        end
else 
        for thisline=1,tonumber(self.mat.numlin) do
                local theline=string.format("%s%d%s","BGCOLOR",thisline,":*")
                self.mat[theline]="254 254 254"
        end
end


self.mat.redraw="yes"

end




function miup:mat_connect(pre_post,lincol)
if self[pre_post] then
        local t=self[pre_post]
        for k,v in pairs(t) do
        
                if t[k][1]=="method" then

                        if t[k][4]~=nil then
                                t[k][2][t[k][3]](t[k][2],t[k][4],lincol)
                        end

                elseif t[k][1]=="store" then
			
			local valtostore=tonumber(self.mat:getcell(lincol[1],lincol[2]))

                       
--                        local idx=copy(t[k][3])
			local idx={}
                        for k,v in pairs (t[k][3]) do
       				idx[k]=v               
--	         	if type(v)=="table" then 
--                                        idx[k]=v.val
--                                else
--                                        idx[k]=v
--                                end
                        end
			
                        table.insert(idx,lincol[1])
                        table.insert(idx,lincol[2])

                        t[k][2]:store(valtostore,idx)
                       


                end

        end
end
end


--------------------------------------
--Pour appeller une fonction concernant un objet depuis l'extérieur (connect) il faut le faire avec miup.resizemat(self)  et non pas miup:resizemat (va savoir pourquoi)  sinon boucle infinie d'erreur
--ICI DONC la fonction pour redimensionner la matrice deuis mes number box de lignbr et colnbr


--Cette fonction  pour remettre les numéros de lignes selon le numlin et redimensionner les lignes
function miup.resizemat(self)
for adx=0,self.mat.numlin do self.mat:setcell(adx,0,tostring(adx)) end
self.mat.redraw="yes"

end







-------------- Used to store the content of a matrix (array) in a table
function miup:storemat(tab)
print("STOREMAT",#tab)
	self.mat.editmode="no"


	local oself=self        

        local tabto=tab[1]


        local idx={}
        local idtaille={}

	for k,v in pairs(tab) do
		if type(k)=="number" and k>1 then 
        		idx[k-1]=v
--       			idtaille[k-1]=v
		end
	end





	local ligidx=#idx+1
	local colidx=#idx+2

--[[
table.insert(idtaille,"lin")
tabto:store(tonumber(self.mat.numlin),idtaille)
idtaille[#idtaille]="col"
tabto:store(tonumber(self.mat.numcol),idtaille)
]]--


	for lin=1,self.mat.numlin do
        	for col=1,self.mat.numcol do
        	        idx[ligidx]=lin
        	        idx[colidx]=col
        	        local val=tonumber(self.mat:getcell(lin,col))                       
        	        if val~=nil then
        	                tabto:store(val,idx)
        	        else
        	                tabto:store(nil,idx)
        	        end
        	end
	
	end

end





function miup.clearall(self)
self.mat.CLEARVALUE="CONTENTS"
end






-- Used to synchronize the scrollbar of several matrixes (only for vertical, to use horizontal change "*:%d"
function miup:matsync(tosync)


function self.mat:scrolltop_cb(lin,col)

       local toshow=string.format("%d:*",lin)
if tosync.mat then
       tosync.mat.ORIGIN=toshow
elseif tosync[1].mat then
	for k,v in pairs(tosync) do
		if type(k)=="number" then
		tosync[k].mat.ORIGIN=toshow
		end
	end


end

end
end



function miup.mat_resize_col_lig(self,tab)

local sqnb=tonumber(tab[1].txt.value)
local tknb=tonumber(tab[2].txt.value)
local lgnb=taille[sqnb][tknb][1]
local clnb=taille[sqnb][tknb][2]

lignbr.txt.value=lgnb
colnbr.txt.value=clnb

end





function miup.track_colorline(self,gridstep)
for k,v in pairs(self) do
	if type(k)=="number" then
	
	self[k].mat_colorline(self[k],gridstep)

	end
end
end






function miup.matrecall(self,tab)        

	self.clearall(self)

local myself=mutil:get_table(tab)

--print("essai",myself[1][1],myself[1][2],myself[1][3])
--	local myself=tab[1]




--print("size",#myself,#myself[1])
--print("FINDSEQ",#tab,getval(tab[2]),getval(tab[3]))
if myself and myself~=nil then
        if not myself.lin then
                myself.lin=self.initlin
		print(myself.lin)
        end

        if not myself.col then
                myself.col=self.initcol
		print(myself.col)
        end

self.resizemat(self)

--if myself~=nil then
        for l=1,tonumber(self.mat.numlin) do
                for c=1,tonumber(self.mat.numcol) do
                        if myself[l] and myself[l][c] then
--print("matrecall",l,c)
                        self.mat:setcell(l,c,tonumber(myself[l][c]))
                        end
                end
        end
end
self.mat.redraw="yes"
--print("LEAVEITEM",tab[#tab])
end











-------------------------------------------------------------------------------------------------------
--------------------------TRACKS - MULTIPLE MATRIX INSTANTIATE AT RUNTIME ------------------------------
--------------------------------------------------------------------------------------------------------



function miup:t_show(state)
if state==1 then
	self.mat.numcol=self.col
        local sze=tonumber(self.mat.widthdef)*(tonumber(self.mat.numcol)+(tonumber(self.mat.numcol)*0.115))+25
	local str=sze .. "x"
	self.mat.size=str
	iup.Refresh(self.vbx)
	self.matrecall(self,self.storetab)

elseif state==0 then
print("TSHOW",self.visible)
	self.mat.numcol=self.visible

	local sze=(tonumber(self.mat.widthdef)*(tonumber(self.mat.numcol)+1))
	local str=sze .. "x"
	self.mat.size=str
	self.mat.expand="vertical"
	iup.Refresh(self.vbx)
end


end


function miup:t_pass_connect(idx)

local function adapt(pre_post)
	if self[pre_post] then
		
local tab={}
			for k,v in pairs(self[pre_post]) do
tab[k]=mutil:copy_ref(v)	
				for k1,v1 in pairs(v) do
					if v1=="self" then 
						tab[k][k1]=self[idx] 
					elseif v1=="idx" then
						tab[k][k1]=self[idx].idx
					elseif type(v1)=="table" then
						for k2,v2 in pairs(v1) do
							if v2=="idx" then
								tab[k][k1][k2]=idx
							
							end
						end
					end
		
			
				end
			end

return tab
	end

end

self[idx].preconnect=adapt("preconnect")
self[idx].postconnect=adapt("postconnect")

end






------------------------- MAIN TRACK _ CALLED FOR TRACK SYSTEM CREATION ------------------------
function miup:mat_tracks(col,lig,stab)
	local oself=self
	self.copy_buffer=miup:new()
	self.val=1

	self.cur_label=iup.label{title="current " .. self.val}
	self.nb_of_tracks=1
	self.visible=3

	self[1]=self:new()
	self[1].idx=1


	self.storetab=mutil:copy_ref(stab)
	self[1].storetab=mutil:copy_ref(stab)
	table.insert(self[1].storetab,1)
	
	self.initcol=col
	self.initlin=lig


	self[1].col=col
	self[1]:matrix(col,lig,self[1].storetab)

	self:t_pass_connect(1)
	
	
	self[1].mat.numcol=self.visible
	self[1].mat.size="188x"
	self[1].mat.expand="VERTICAL"
	self[1].mat.scrollbar="vertical"
	self[1].tog=iup.toggle{title="Show",action=function(me,state)
		oself[1]:t_show(state)
		 end}

	self[1].col_nbx=miup:new()

	self[1].col_nbx:nbx(self.initlin,3,false,0,1,1)

	local st,tb=mutil:get_store_vals(self[1].storetab)
	table.insert(tb,"col")
	self[1].col_nbx.postconnect={
		{"val",self[1],"col"},
		{"store",st,self[1].col_nbx,tb},
		{"method",self[1],"track_colresize"}
	}
	self[1].vbx=iup.vbox{
				iup.frame{
			iup.hbox{self[1].clearbut,self[1].tog,self[1].col_nbx.nbx,self.cur_label}
				;title="Track_1"},
				self[1].mat
}
--[[
	self[1].vbx=iup.vbox{
	iup.hbox{
		iup.vbox{
			iup.label{title="Track_1"},
			self[1].clearbut
	},
		self[1].tog,
		self[1].col_nbx.nbx,
		self.cur_label,
	},
	self[1].mat}
]]--
	self.conteneur=iup.hbox{self[1].vbx,expandchildren="yes",expand="yes"}
	self[1]:storemat(self[1].storetab)
	self.track_set_current(self,1)
end
	
	







------------------ 


function miup.mat_tracks_change(self,nbr)
local nb=nbr.val
--local pth=terralib.includec("pthread.h")

if nb>self.nb_of_tracks then


	for iter=self.nb_of_tracks+1,nb do

			print("GARBAGE BEFORE",collectgarbage("count"))
		local lig=self[1].mat.numlin
		self.val=iter
		self[iter]=self:new()
		self[iter].idx=iter

	
		self[iter].storetab=mutil:copy_ref(self.storetab)
		table.insert(self[iter].storetab,iter)
		self[iter]:matrix(self.initcol,lig,self[iter].storetab)
	
		self:t_pass_connect(iter)	
		self.track_set_current(self,iter)
		self[iter].mat.numcol=self.visible
		self[iter].col=self.initcol
		self[iter].mat.size="188x"
		self[iter].mat.expand="VERTICAL"
		self[iter].mat.scrollbar="vertical"
	
		self[iter]:storemat(self[iter].storetab)
		self[iter].tog=iup.toggle{title="Show",action=function(me,state) self[iter]:t_show(state) end}	
		self[iter].col_nbx=miup:new()
		self[iter].col_nbx:nbx(self.initcol,3,false,0,1,1)



		local st,tb=mutil:get_store_vals(self[iter].storetab)
		table.insert(tb,"col")

	self[iter].col_nbx.postconnect={
		{"val",self[iter],"col"},
		{"store",st,self[iter].col_nbx,tb},
		{"method",self[iter],"track_colresize"}
}


		local ttl="Track_" .. iter

	self[iter].vbx=iup.vbox{
				iup.frame{
			iup.hbox{self[iter].clearbut,self[iter].tog,self[iter].col_nbx.nbx}
				;title=ttl},
				self[iter].mat
}
--[[
		self[iter].vbx=iup.vbox{
		iup.hbox{
			iup.vbox{
				iup.label{title=ttl},
				self[iter].clearbut
			},
			self[iter].tog,
			self[iter].col_nbx.nbx,
		},
		self[iter].mat}
]]--

		iup.Append(self.conteneur,self[iter].vbx)
		iup.Map(self[iter].vbx)
		iup.Refresh(self[iter].vbx)

			print("GARBAGE AFTER",collectgarbage("count"))
	end

	self.nb_of_tracks=nb
	--iup.Redraw(self.conteneur,1)

elseif nb<self.nb_of_tracks then
	local str="you're going to delete tracks"
	for a=nb+1,self.nb_of_tracks do 
		str=str .. string.char(32) .. a
	end
	str=str .. string.char(10) .. "Are you sure ?"

	local but=iup.Alarm("DELETE TRACKS",str,"Yes","No")
	
	if but==1 then

		for iter=nb+1,self.nb_of_tracks do

			print("GARBAGE BEFORE",collectgarbage("count"))
			local id_stab=self[iter].storetab[1]
			local stab=mutil:copy_ref(self[iter].storetab)
			table.remove(stab,1)	
			
			for ki,vi in pairs(id_stab) do 
				if type(ki)=="number" then 
					id_stab:mydestroy({ki,iter})
				end
			end


--			iup.Detach(self[iter].vbx)
--			iup.Unmap(self[iter].vbx)
--			self[iter].storetab[1]:mydestroy({self[iter].storetab[2]})
			self[iter].vbx:destroy()

			self[iter].mat=nil
			self[iter].nbx=nil
			self[iter].tog=nil
			self[iter].vbx=nil
			self[iter]=nil
			collectgarbage()

			print("GARBAGE",collectgarbage("count"))
			iup.Refresh(self.conteneur)
	
		end
	

		self.nb_of_tracks=nb

	elseif but==2 then
		nbr.assign(nbr,{self.nb_of_tracks})
	end

end
end





function miup.track_resize(self,nb)
for k,v in pairs(self) do
	if type(k)=="number" then
		self[k].mat.numlin=nb.val
		self[k]:resizemat(self,nb.val)
		self[k].mat.redraw="yes"
	end
end
end




function miup.track_colresize(self,nb)
if self.tog.value=="ON" then
self:t_show(1)
end
end



function miup.track_recall(self,tab)

for k,v in pairs(self) do
	if type(k)=="number" and self[k] then
		self.val=k

local tb=mutil:copy_ref(tab)
table.insert(tb,"col")

		self[k].col_nbx.assign(self[k].col_nbx,tb)
		self[k].matrecall(self[k],self[k].storetab)
	end
end

end




function miup.track_store_all(self)
for k,v in pairs(self) do
	if type(k)=="number" and self[k] then
		self[k]:storemat(self[k].storetab)
	end
end


end



function miup.track_col_visible(self,nbr)
	self.visible=mutil:getval(nbr)

	for k,v in pairs(self) do
		print("kkkk",k)
		if type(k)=="number" and self[k]  then
			self[k]:t_show(0)
		end
	end

end


function miup.track_set_current(self,id)
	print("ENTERITEM",id)
	self.val=self[id].idx
	self.cur_label.title="current " .. id
end


function miup.track_clear(self,tab)
	if not tab then
		for k,v in pairs(self) do 
			if type(k)=="number" and self[k].mat then
				self[k].clearall(self[k])
			end
		end

	else

		for k,v in pairs(tab) do
			if type(v)=="number" and self[v].mat then
				self[v].clearall(self[v])
			end
		end

	end

end







































------------------------------------------------------------------------------------------------------
--ICI FONCTION PLOT DRAW :
--------------------------------------------------------------------------------------------------------

--Reste à faire 
--Pour le storage et la conversion en GEN16, il faut un bouton de "number of points" et vérifier que ça tombe juste, exactemeent. Voir ca aussi pour l'affichage
--Intégration des courbes de Béziers -- et GEN08 ' 



function miup:plotdraw(ymin,ymax,ystep,xstep)


	local oself=self
	self.clearbut=iup.button{title="Clear"}
	
	function self.clearbut:action()
		oself:plotclear()
	end


	self.storebut=iup.button{title="Store"}
	function self.storebut:action()
	        local dscount=tonumber(oself.plot.DS_COUNT)
		if dscount>1 then

		        for scnt=0,dscount-1 do
	                        local crv
	                       local xa,ya=oself.plot:GetSample(0,scnt)
	                       if oself.curvetab[scnt] then crv=oself.curvetab[scnt] end
		               local sp={xa,ya,crv}
		        end
		end
	end


	self.gen16=iup.toggle{title="GEN16"}
	self.genBezier=iup.toggle{title="GENQuadBezier"}
	self.radio_box=iup.hbox{self.gen16,self.genBezier}
	self.radio_mode=iup.radio{self.radio_box}

	local function radio_action(id)
		self.plotclear(self)
	end

	function self.gen16:action()
		if self.value=="ON" then
			radio_action(self)
		end
	end

	function self.genBezier:action()
		if self.value=="ON" then
			radio_action(self)
		end
	end



-- ICI LE PLOT EN LUI MEME 


	local domain=16384
	self.curvetab={}



	local xmin,xmax=0,1
	--local xmin,xmax,ymin,ymax,xstep,ystep=0,1,0,1,0.1,0.1
	local a,b,z,dsfound,sampfound,seg,segds,segsamp1,segsamp2,xstat
	local position=1

	local xm,ym,rm
	local yclick

	local ftab={}
	local mx=iup.matrixex{}


	self.plot = iup.plot{
	    HIGHLIGHTMODE="SAMPLE", --could be "BOTH" for curve and sample
	    MARGINBOTTOM = 30,
	    MENUCONTEXT="YES",
	    READONLY="NO",
	    SCREENTOLERENCE=10,
	    MENUITEMPROPERTIES="YES",
	    PLOT_CURRENT=0,
	    EDITABLEVALUES="yes",
	    AXS_XAUTOMIN="NO",
	    AXS_YAUTOMIN="NO",
	    AXS_XAUTOMAX="NO",
	    AXS_YAUTOMAX="NO",
	    AXS_XMAX=xmax,
	    AXS_XMIN=xmin,
	    AXS_YMAX=ymax,
	    AXS_YMIN=ymin,
	    AXS_XTICK="YES",
	    AXS_YTICK="YES",
	    BOX="YES",
	    BOXLINEWITDH=2,
	    GRID="YES",
	    DS_COLOR="255 0 255",
	    DS_MARKSTYLE="CIRCLE",
	    AXS_XTICKAUTO="NO",
	    AXS_YTICKAUTO="NO",
	    AXS_XTICKMAJORSPAN=xstep,
	    AXS_YTICKMAJORSPAN=ystep,
	    AXS_XTICKMAJORSIZE=8,
	    AXS_YTICKMAJORSIZE=8,
	    AXS_XTICKFORMATAUTO="NO",
	    AXS_YTICKFORMATAUTO="NO",
	    AXS_XTICKFORMAT="%.01f",
	    AXS_YTICKFORMAT="%.01f",
	--    AXS_XTICKFORMATPRECISION="%.01f",
	--    AXS_YTICKFORMATPRECISION="%.01f",
	    DATASETCLIPPING="NONE",
	    FORMULA_PARAMETRIC="yes",
	    size="x150",
	    BGCOLOR="45 26 28",
	    GRAPHICSMODE="NATIVEPLUS"
}


self.plot:Begin(0)
self.plot:End()

	local function curve(beg,ending,dur,idx,typ)
		local res
        	if typ==0 then
        	        --res=idx
	                res=beg+(idx*((ending-beg)/dur))
	        
	        else
	        --local dur=ending-beg
	
	                if typ>10 then typ=10
	                elseif typ<-10 then typ=-10 
	                end

	         res=beg+(ending-beg)*(1-math.exp(idx*typ/(dur-1)))/(1-math.exp(typ))
	        end
        	return res
	end

	self.plot.CURRENT=0
	self.plot.DS_MODE="MARK"
	self.plot.DS_MARKSTYLE="DIAMOND"











-------------------------------------------FUNCTIONS USED BY CALLBACKS



	local function m_motion(self,xm,ym,rm)

		if xm>tonumber(self.AXS_XMAX) then xm=tonumber(self.AXS_XMAX) end
		if xm<tonumber(self.AXS_XMIN) then xm=tonumber(self.AXS_XMIN) end
		if ym>tonumber(self.AXS_YMAX) then ym=tonumber(self.AXS_YMAX) end
		if ym<tonumber(self.AXS_YMIN) then ym=tonumber(self.AXS_YMIN) end
	 if string.find(rm,"1%s+A") then
		 local compte=tonumber(self.DS_COUNT)
		 local prevsamp
		 for a=0,compte-1 do
			 local xd,yd=self:GetSample(0,a)
			 if xd>xm then prevsamp=a-1 break end
		 end
		 if prevsamp and prevsamp<compte-1 then
			if prevsamp and oself.curvetab[prevsamp]==nil then oself.curvetab[prevsamp]=0 end 
			 
			oself.curvetab[prevsamp]=oself.curvetab[prevsamp]+((ym/tonumber(oself.plot.AXS_YMAX))-(yclick/tonumber(oself.plot.AXS_YMAX)))
		       
			if oself.curvetab[prevsamp]>10 then oself.curvetab[prevsamp]=10
			elseif oself.curvetab[prevsamp]<-10 then oself.curvetab[prevsamp]=-10
			end
		 end
	self.redraw="yes"
	end
	       
		
		
	if sampfound~=nil and dsfound==0 then
		local prevx,prevy,aftx,afty=0,0,1,1
	if sampfound>0 then prevx,prevy=self:GetSample(0,sampfound-1) end
	if sampfound<tonumber(self.DS_COUNT)-1 then aftx,afty=self:GetSample(0,sampfound+1) end
	      
		     if self:GetSampleSelection(0,sampfound)==true then   
			if xm>prevx and xm<aftx then
	     
			self:SetSample(0,sampfound,xm,ym)
			self.redraw="yes"
			elseif sampfound>0 and xm<prevx then
			self:SetSampleSelection(0,sampfound,0)
			self:SetSample(0,sampfound,prevx,prevy)
			local prevcurve,thiscurve=oself.curvetab[sampfound-1],oself.curvetab[sampfound]
			oself.curvetab[sampfound-1]=thiscurve
			oself.curvetab[sampfound]=prevcurve
			sampfound=sampfound-1
			self:SetSampleSelection(0,sampfound,1)
			
			elseif sampfound<tonumber(self.DS_COUNT)-1 and xm>aftx then 
			self:SetSampleSelection(0,sampfound,0)
			self:SetSample(0,sampfound,aftx,afty)
			local thiscurve,nextcurve=oself.curvetab[sampfound],oself.curvetab[sampfound+1]
			oself.curvetab[sampfound]=nextcurve
			oself.curvetab[sampfound+1]=thiscurve
			sampfound=sampfound+1
			self:SetSampleSelection(0,sampfound,1)
			end
		     end
	end
	end



	local function m_button(self,but,press,x,y,stat)
	print(but,press,x,y,stat)
		if x>tonumber(self.AXS_XMAX) then x=tonumber(self.AXS_XMAX) end
		if x<tonumber(self.AXS_XMIN) then x=tonumber(self.AXS_XMIN) end
		if y>tonumber(self.AXS_YMAX) then y=tonumber(self.AXS_YMAX) end
		if y<tonumber(self.AXS_YMIN) then y=tonumber(self.AXS_YMIN) end

	if press==1 then yclick=y end

		a,b=self:Transform(x,y)
	      z,dsfound,sampfound=self:FindSample(a,b)


	seg,segds,segsamp1,segsamp2=iup.PlotFindSegment(self,a,b)


	--If double click on a sample then it snaps the sample to the grid 
	if press==1 and but==49 and string.find(stat,"1%s+D") and z==1 and dsfound==0 then
		
	local newx=self.AXS_XMAX/x

	local nextx,nexty

	for m=xmin,xmax+tonumber(self.AXS_XTICKMAJORSPAN),tonumber(string.format("%f",self.AXS_XTICKMAJORSPAN)) do
		if m>x then
		       local upx=m-x
		       local dnx=x-(m-tonumber(self.AXS_XTICKMAJORSPAN))
			if dnx<upx then nextx=m-tonumber(self.AXS_XTICKMAJORSPAN)
			elseif upx<dnx then nextx=m
				break

			end
			break
		end

	end

	for n=tonumber(self.AXS_YMIN),tonumber(self.AXS_YMAX)+tonumber(self.AXS_YTICKMAJORSPAN),tonumber(string.format("%f",self.AXS_YTICKMAJORSPAN)) do
		if n>y then 
		       local upy=n-y
		       local dny=y-(n-tonumber(string.format("%f",self.AXS_YTICKMAJORSPAN)))
			if dny<upy then nexty=n-tonumber(string.format("%f",self.AXS_YTICKMAJORSPAN))
			elseif upy<dny then nexty=n
				break
			end
			break
		end
	end



	---- Here i need to make a condition - to reparse dataset and redraw if, for example, the snap to grid goes after another point on the dataset


		

	local dscnt=tonumber(self.DS_COUNT)-1
	local flg=false
	if nextx and nexty then
		if dscnt>sampfound then
		local tox,toy=self:GetSample(dsfound,sampfound+1)
			if tox<nextx then
			local this_crv,to_crv=oself.curvetab[sampfound],oself.curvetab[sampfound+1]
			self:SetSample(dsfound,sampfound+1,nextx,nexty)
			self:SetSample(dsfound,sampfound,tox,toy)
			oself.curvetab[sampfound]=to_crv
			oself.curvetab[sampfound+1]=this_crv
			flg=true
			end
		end
		if sampfound>0 then
		local tox,toy=self:GetSample(dsfound,sampfound-1)
			if tox>nextx then 
			local this_crv,to_crv=oself.curvetab[sampfound],oself.curvetab[sampfound-1]
			self:SetSample(dsfound,sampfound-1,nextx,nexty)
			self:SetSample(dsfound,sampfound,tox,toy)
			oself.curvetab[sampfound]=to_crv
			oself.curvetab[sampfound-1]=this_crv
			flg=true
			end
		end
		if flg==false then
			self:SetSample(dsfound,sampfound,nextx,nexty)
		end
	       flg=false 
	end
	self.redraw="yes"

	--        self:SetSample(0,sampfound,nextx,nexty)





	-- If alt+clic on a sample - then it removes the sample
	elseif press==1 and but==49 and string.find(stat,"1%s+A") and z==1 then
		local ct=tonumber(oself.plot.DS_COUNT)
		self.DS_REMOVE=sampfound
	       if ct>sampfound then
		       for a=sampfound+1,ct do
				oself.curvetab[a-1]=oself.curvetab[a]
			end
		end
	       self.redraw="yes"

	end

	-- If clic and no point is on the zone, it will create a sample
	if press==1 and string.find(stat,"C1")==nil and string.find(stat,"S")==nil  and z~=1 and string.find(stat,"1%s+A")==nil then
		if tonumber(self.DS_COUNT)>0 then
		   for cnt=0,tonumber(self.DS_COUNT)-1 do
			local tx,ty=self:GetSample(0,cnt)

			if tx>x then 
			  position=cnt
			  break
			elseif tx<=x and cnt==self.DS_COUNT-1 then
				position=self.DS_COUNT
			end
		   end
		elseif tonumber(self.DS_COUNT)<1 then 
			position=0
		end
			
		     self:Insert(0,position,x,y)
		     table.insert(oself.curvetab,position,0)
		     --oself.curvetab[position]=0
		     self.redraw="yes"
		     self:SetSampleSelection(0,position,1)
		     z,dsfound,sampfound=self:FindSample(a,b)
	--If clic and point on the zone, then select the sample
		elseif press==1 and z==1 and dsfound==0 then 
		     self:SetSampleSelection(dsfound,sampfound,1)
	--unselected on mouse release
		elseif press==0 then
			if sampfound then
				self:SetSampleSelection(0,sampfound,0)

			end
		end


	end





	local function m_predraw(self,canvas)
		if self.gen16.value=="ON" then	
			self.CURRENT=0
			local compte=tonumber(self.DS_COUNT)
			if compte>1 then
			local taby={}
				for dr=0,compte-1 do
					
					if dr<compte-1 then
						local begx,begy=self:GetSample(0,dr)
						local fbx,fby=self:GetSample(0,dr+1)
						if oself.curvetab[dr]==nil then oself.curvetab[dr]=0 end
						local crv=oself.curvetab[dr]
						local base1_begx=begx/xmax
						local base1_fbx=fbx/xmax        
						local basedom_begx=math.floor(base1_begx*domain)
						local basedom_fbx=math.floor(base1_fbx*domain)
			
						local ampx=math.abs(basedom_begx-basedom_fbx)
						local ampy=math.floor(math.abs(begy-fby))
						
						for zeidx=0,ampx do
							local cr
							if begy<fby then cr=crv*-1
							else cr=crv
							end
							local y=curve(begy,fby,ampx,zeidx,cr)
							--val=((y/domain)*ymax)+begy
							taby[zeidx+basedom_begx]=y
						end
				  

					end

				end 

			 
			canvas:Foreground(cd.EncodeColor(0,0,255))
				for k,v in pairs(taby) do                   
					local ax,ay=iup.PlotTransform(self,k/domain,v)
					if taby[k+1] then local ax2,ay2=iup.PlotTransform(self,(k+1)/domain,taby[k+1])
					canvas:fLine(ax,ay,ax2,ay2)
					else
					canvas:fLine(ax,ay,ax,ay)
					end
				end

			end

		elseif self.genBezier.value=="ON" then
			--Quadbezier : B(t)=(1-t)carré*P0+2t*(1-t)*P1+tcarré*P2, t appartient [0,1]


		end


	end



	function self.plot:plotbutton_cb(but,press,x,y,stat)
		m_button(self,but,press,x,y,stat)        
	end
	

	function self.plot:plotmotion_cb(x,y,r)
	        xm,ym,rm=x,y,r
	        m_motion(self,x,y,r)
	end





	------------------------------- PREDRAW CALLBACK
	function self.plot:predraw_cb(canvas)
		m_predraw(self,canvas)
	end





end







function miup.plotredraw(self)
	self.plot.redraw="yes"
end



function miup.plotclear(self)
	for cnt=0,tonumber(self.plot.DS_COUNT)-1 do
	        self.plot.DS_REMOVE=cnt
	end
	self.plot.redraw="yes"
	self.curvetab={}
end



function miup:plotclear()
	for cnt=0,tonumber(self.plot.DS_COUNT)-1 do 
	        self.plot.DS_REMOVE=cnt
	end
	self.plot.redraw="yes"
	self.curvetab={}
end


function miup:plotstore(tab,numero)
	if self.plot then
        local dataset=tonumber(self.plot.DS_COUNT)-1

	        tab[numero.val]=miup:new()
	        tab[numero.val].functype="plotdraw"
	        tab[numero.val].y_min=tonumber(self.plot.AXS_YMIN)
	        tab[numero.val].y_max=tonumber(self.plot.AXS_YMAX)
	        tab[numero.val].xgrid=tonumber(self.plot.AXS_XTICKMAJORSPAN)
	        tab[numero.val].ygrid=tonumber(self.plot.AXS_YTICKMAJORSPAN)
	        tab[numero.val].data_size=dataset
        	for it=0,dataset do
        	   local sx,sy,sz=nil,nil,nil
        	   sx,sy=self.plot:GetSample(0,it)
        	        if self.curvetab and self.curvetab[it]~=nil and self.curvetab[it]~=0 then
        	                sz=self.curvetab[it]
        	        else
        	                sz=0
        	        end
        	        tab[numero.val][it]={sx,sy,sz}
        	end

	
	end
end











------------------------------
--Store Waveforms built in the array
------------------------------
function miup:waveform_store(tab,numero)
        if self.mat then
		if self.mode=="GEN10" then		

                	local numlin=self.mat.numlin
                	tab[numero.val]=miup:new()
                	tab[numero.val].functype="waveform"
               		tab[numero.val].ligne=tonumber(numlin)              
                	
			for ln=1,numlin do 
                	        local a,b,c=tonumber(self.mat:getcell(ln,1)),tonumber(self.mat:getcell(ln,2)),tonumber(self.mat:getcell(ln,3))
                	        tab[numero.val][ln]={a,b,c}
                	end
		elseif self.mode=="GEN2" then
			local numlin=self.mat.numlin
			tab[numero.val]=miup:new()
			tab[numero.val].functype="gentable"
			tab[numero.val].ligne=tonumber(numlin)
		
			for ln=1,numlin do
				local a=tonumber(self.mat:getcell(ln,1))
				tab[numero.val][ln]=a
			end

       		end
	end
end











---- Celle-ci peut être générique
function miup.funcrecall(self,tab)

        
local myself
        local mytable=self
        local number=tab[2].val
        if mytable[number] then

local fctype=mytable[number].functype
        myself=funcparse[fctype]
        if mytable[number].functype=="plotdraw" then
                
                for k,v in pairs(mytable[number]) do
                           if type(k)=="number" and mytable[number][k] and type(mytable[number][k])=="table" then
                                
                                myself.plot:Insert(0,k,mytable[number][k][1],mytable[number][k][2])   
                                myself.curvetab[k]=mytable[number][k][3]
                           end
                end

               myself.plot.redraw="yes" 
        elseif mytable[number].functype=="sample" then
                myself.wread(myself,mytable[number].sample)
        elseif mytable[number].functype=="waveform" then
		--myself.radio.VALUE_HANDLE=myself.tg1
		myself.tg1.value="TOGGLE"
		myself.tog_fct(3,"GEN10")
                if mytable[number].ligne then 
		myself.mat.numlin=mytable[number].ligne 
                myself.mat.redraw="yes"
                end
                 for k,v in pairs(mytable[number]) do
                         if type(k)=="number" and mytable[number][k] and type(mytable[number][k])=="table" then
                                for cols=1,3 do
                                        if mytable[number][k][cols] and type(mytable[number][k][cols])=="number" then
                                        
                                        myself.mat:setcell(k,cols,tostring(mytable[number][k][cols]))
                                        myself.mat.redraw="yes"
                                        end
                                end
                        end
                end
	elseif mytable[number].functype=="gentable" then		

		myself.tg2.value="TOGGLE"
		myself.tog_fct(1,"GEN2")
		--myself.radio.VALUE_HANDLE=myself.tg2
                if mytable[number].ligne then
			myself.mat.numlin=mytable[number].ligne 
                	myself.mat.redraw="yes"
                end
		for k,v in pairs(mytable[number]) do
			if type(k)=="number" then
				myself.mat:setcell(k,1,mytable[number][k])
			end
		end	
                	myself.mat.redraw="yes"
	end
                




end
end























---------------------------------------------------
--BROWSE FILE
----------------------------------------------

function miup:browse(flag)
local oself=self
self.fpath,self.browsestat=iup.GetFile("")

self.browsestat=tonumber(self.browsestat)

if self.browsestat~=-1  then
self.fpath=string.gsub(self.fpath,string.char(92),string.char(47))

if flag and flag=="samp" then
self.wread(self,self.fpath)
end
end
end

























-------------------------------------------
--Save sample
-----------------------------------------



function miup:samplestore(tab,numero)
if self.fpath and type(self.fpath)=="string" then
        tab[numero.val]=miup:new()
        tab[numero.val].functype="sample"
        tab[numero.val].sample=tostring(self.fpath)
end
end







---------------------------------------------------
--SAMPLE PLOT TO SEE WAVEFORMS
----------------------------------------------------

function miup:sampleplot()
	local oself=self
	--self.browsebut=iup.button{title="Browse"}


	--function self.browsebut:action()
	--oself:browse()
	--end


       
self.samp=iup.plot{
	AXS_YMIN=-1,
	AXS_YMAX=1,
	AXS_XMIN=0,
	AXS_XMAX=1,
	}
	self.samp:Begin(0)
	self.samp:End()
	
	self.samp.DS_CURRENT=0
	self.samp.DS_MODE="AREA"

--self.samp.maxsize="1350x200"
end


----------------------------------------------------
--CLEAR SAMPLE
--------------------------------------------------
function miup.spclear(self)
        self.samp.CLEAR="yes"
        self.samp:Begin(0)
        self.samp:End()
	self.samp.redraw="yes"
	self.samp.DS_CURRENT=0
	self.samp.DS_MODE="AREA"
end




--Solution du temps de chargement : 
--Matrices : 


-- a faire : 
-- bien calibrer lamplitude
-- bien calibrer le temps en ms / sec
-- faire des échantillonnages différents selon la taille du fichier

-------------------------------------------
--LECTURE DE SAMPLES ECHANTILLONS SONORES
-----------------------------------------
function miup.wread(self,filepath)
self.spclear(self)
        local tab={}
        local reader=wav.create_context(tostring(filepath),"r")
        local sr=reader.get_sample_rate()
        local samples=reader.get_samples_per_channel()
        local dur=samples/sr
       
	local stepsize=50
	
	local stepnbr=math.floor(samples/stepsize)
	
	local amp=math.pow(2,(reader.get_bits_per_sample()))/2
	--local stepnbr=reader.get_samples_per_channel()/stepnbr
	local pickup=10
	--local step=0
	        for step=0,stepnbr do
	                
	        local cnt=0
	                for z=0,stepsize-1,pickup do
	                        
	                        local position=z+(step*stepsize)
	                        if position< samples-2 then
	                        
	                        reader.set_position(position)
	                        local sq=(math.abs(reader.get_samples(1)[1][1]/amp))*(math.abs(reader.get_samples(1)[1][1]/amp))
	                        cnt=cnt+sq
	                        end
	                        --cnt=cnt+(math.abs(reader.get_samples(1)[1][1])/amp) 
	                end 
	                tab[step]={}
	                tab[step][1]=(step/stepnbr)*dur
	                tab[step][2]=math.sqrt(cnt/(stepsize/pickup))
	                step=step+1
	
	        end
	for k,v in pairs(tab) do
	
	        self.samp:Insert(0,k,tab[k][1],tab[k][2])
	        self.samp:Insert(0,k,tab[k][1],tab[k][2]*-1)
	end
	
	        self.samp.redraw="yes"
end
	
	
	
	





	--[[ Bibliotheque WAV
		Reads or writes audio file in WAVE format with PCM integer samples.
		
		Function 'create_context' requires 2 arguments: a filename and a mode, which can be "r" (read) or "w" (write).
		A call returns one table with methods depending on the used mode.
		On reading, following methods are callable:
		- get_filename()
		- get_mode()
		- get_file_size()
		- get_channels_number()
		- get_sample_rate()
		- get_byte_rate()
		- get_block_align()
		- get_bits_per_sample()
		- get_samples_per_channel()
		- get_sample_from_ms(ms)
		- get_ms_from_sample(sample)
		- get_min_max_amplitude()
		- get_position()
		- set_position(pos)
		- get_samples_interlaced(n)
		- get_samples(n)
		On writing, following methods are callable:
		- get_filename()
		- get_mode()
		- init(channels_number, sample_rate, bits_per_sample)
		- write_samples_interlaced(samples)
		- finish()
		
		(WAVE format: https://ccrma.stanford.edu/courses/422/projects/WaveFormat/)

]]--













-------------- IUP GAUGE - sort of a progressbar used to make a digital viewmeter for Csound
--
--
--
function miup:viewmeter(min,max)


        self.meter=iup.progressbar{}
        if min then self.min=min end
        if max then self.max=max end

end


function miup:viewmeter_changeval(newval)
        
        self.meter.value=newval

end

function miup:viewmeter_getval()
        return tonumber(self.meter.value)
end





function lvmet(lv_val)
if level_master and lv_val>=0 and lv_val<=1 then
        level_master:viewmeter_changeval(lv_val)        

end

end




function levmeterfunc(az)

print("levelmeter",az,type(az))
if level_master and az.levmet>=0 and az.levmet<=1 then
level_master:viewmeter_changeval(a.levmet)
end

end







function miup:scope(size_samp)

        self.samp_nbr=size_samp

        self.sc=iup.plot{
                AXS_YAUTOMIN="YES",
                AXS_YAUTOMAX="YES",
        	AXS_YMIN=-1,
	        AXS_YMAX=1,
	        AXS_XMIN=0,
        	AXS_XMAX=1,
                AXS_X="no",
                AXS_Y="no",
                AXS_XTICK="NO",
                AXS_YTICK="NO",
                HIGHLIGHTMODE="SAMPLE", --could be "BOTH" for curve and sample
                BOX="NO",
                GRID="NO",
	        GRAPHICSMODE="NATIVE",
	}
        self.sc:Begin(0)
        self.sc:End()

        for iter=0,size_samp-1 do
               self.sc:Insert(0,iter,iter,0)
        end
        self.sc.redraw="yes"
        self.buf=mutil:createbuffer(self.samp_nbr,0)

end




function miup:scope_write(val)

        mutil:writebuffer(self.buf,val)
       
 
        for iter=0,self.samp_nbr-1 do

                local tow=mutil:readbuffer(self.buf,"LIFO") 
                self.sc:SetSample(0,iter,iter,tow)

        end

        self.sc.redraw="yes"


end





function miup:scope_init()

self.buf=mutil:clearbuffer(self.buf)
        
        for iter=0,self.samp_nbr-1 do
                self.sc:SetSample(0,iter,iter,0)

        end
self.sc.redraw="yes"

end




-------------------------------------------------------------
--DATA STORAGE ET CONSULTING
-----------------------------------------------------------



--This function is used to store a data in a table at any depth
--if this depth doesnt exist, it will be created
--first argument is the value to store
--in table argument you pass a table containing the keys where you want to store the value
--the miup is replaced by the storage table name
--The count is not used by the used, but is used for recursion
--for example : 
--local table=miup:new()
--table:store("val to store",{"here","hereagain"})
--the value will be stored at table.here.hereagain


function miup:store(value,tab,count)
        
        local cnt
        if not count then cnt=1

		if #tab>=3 then
		end
	else cnt=count

        end

        local  idx=tab

        local cur=mutil:getval(idx[cnt])

        if cnt<#idx then
                if self[cur]==nil then 
                        self[cur]=miup:new()
                end
                        self[cur]:store(value,idx,cnt+1)
        else
                self[cur]=mutil:getval(value)
        end
end


function miup:copy(obj, seen)
  if type(obj) ~= 'table' then return obj end
  if seen and seen[obj] then return seen[obj] end
  local s = seen or {}
  local res = setmetatable({}, getmetatable(obj))
  s[obj] = res
  for k, v in pairs(obj) do res[copy(k, s)] = copy(v, s) end
  return res
end


function miup:table_copy(from)
	for k,v in pairs(from) do
		if type(v)=="table" then
			self[k]=miup:new()
			self[k]:table_copy(from[k])
		else
			self[k]=v
		end
	end


end

--This function is useful to consult a data in a table, at any depth
--not really working now

function miup:consult(tab,count)
local idx=tab
local cnt
if not count then cnt=1
else cnt=count
end
local cur=idx[cnt]

if cnt<=#idx and type(self[cur])=="table" then
       
       
       self[cur]:consult(idx,cnt+1)
else


return self[cur]
end
end


--This function destroys a table and its content
function miup:mydestroy(tab,count)
	local idx=tab
	local cnt
	if not count then cnt=1
	else cnt=count
	end

	local cur=idx[cnt]
	if cnt<#idx and self[cur] then 
	        self[cur]:mydestroy(idx,cnt+1)
	else 
	        if self[cur] then
	        self[cur]=nil
		collectgarbage("collect")
	        end
	end

end



function miup:clear_content()
	for k,v in pairs(self) do
		self:mydestroy({k})
	end

end



--this function finds the maximum key of a table
function miup.findmax(self)
   if not self.maxval then 
           self.maxval=self.val
   elseif self.maxval then
           if self.val>self.maxval then self.maxval=self.val 
           end

   end


end



















-------------------------------- Those two functions are used to save tables as text files  in the format that can be load from MIUP




function miup:save(tab,path)


local jo=io.open(path,"w")
for k,v in pairs(tab) do
local tbcnt={}



local function table_saving(tab,tbcnt,name,count)

local cnt
if not count then cnt=1
else cnt=count
end


for k,v in pairs(tab) do
        if #tbcnt>cnt then 
                for za=cnt,#tbcnt do tbcnt[za]=nil end
        end
        tbcnt[cnt]=k
        if type(v)=="table" then
                
                table_saving(tab[k],tbcnt,name,cnt+1)
        else
                local str=string.format("%s val=",name)
                local tabto="tab="
                for ki,vi in pairs(tbcnt) do 
                        if type(vi)=="string" then tabto=string.format("%s %c%s%c",tabto,34,vi,34)
                        else tabto=string.format("%s %s",tabto,tostring(vi)) end
                       
                end

                local tow,flg
                if type(v)=="string" then
                        tow=string.format("%c%s%c",34,v,34)
                        flg=true        
                elseif type(v)=="number" then 
                        tow=tostring(v)
                        flg=true
                else
                        flg=false
                end
                tow=string.format("%s%s %s",str,tow,tabto)
                if flg==true then
                jo:write(tow)
                jo:write(string.char(10))
                end
                
                
        end
end

end








table_saving(v[1],tbcnt,v[2])
end

jo:flush()
jo:close()


end








------- This Function is used to load the datas

function miup:loader(line)
        local load_exec=loadstring(line)
load_exec()


end
































































































































































---------------------------------------------------------------------------------------------------------------------
--JO_TRACKER
----------------------------------------------------------------------------------------------------------------------
--Here are the functions designed only for my software - its useless for another use
--
--First the functions to format the p2 and p3 instructions  of i statements in the tracker


function miup.format_tracker(self,tab,lincol)
        local mseq,seq,track,lig,col=tab[1],mutil:getval(tab[2]),mutil:getval(tab[3]),lincol[1],lincol[2]
print("init sq track lig col",seq,track,lig,col)
local p1=tonumber(self.mat:getcell(lig,1))
local nlin,ncol=tonumber(self.mat.numlin),tonumber(self.mat.numcol)
if not p1 then
print("erase")

        for it=1,ncol do
                self.mat:setcell(lig,it,nil)
                mseq:store(nil,{seq,track,lig,it})
        end
        
else
        local p2=tonumber(self.mat:getcell(lig,2))
        local p3=tonumber(self.mat:getcell(lig,3))
        if not p2 then
       	print("create p2")         
	print("p2 / seq",seq,"track",track,"lig",lig)
	
		self.mat:setcell(lig,2,0)
                mseq:store(0,{seq,track,lig,2})
	print("check",mseq[seq][track][lig][2])
        end
       	if not p3 then
	print("create p3")
                self.mat:setcell(lig,3,1)
                mseq:store(1,{seq,track,lig,3})
        end

        
print("pchamp",p1,p2,p3,lig)
print("consult",mseq[seq][track][lig][2],mseq[seq][track][lig][3])

end
if mseq and mseq[seq] and mseq[seq][track] and mseq[seq][track][lig] and mseq[seq][track][lig][1] then
local newp3,previous=mseq:find_nextprev(seq,track,lig,"next")

if newp3 and type(newp3)=="number" then
        mseq:store(newp3,{seq,track,lig,3})
end
if previous and previous.p3 and type(previous.p3)=="number" then

        mseq:store(previous.p3,{previous.seq,track,previous.lig,3})
end
end
end





function miup:find_nextprev(seq,track,lig)
local p3

local p1=self[seq][track][lig][1]
local prev={}
local nextone={}

        prev.diff=0
        for sqprev=seq,1,-1 do

                local ligtoiter
                if sqprev==seq then ligtoiter=lig-1
                elseif self[sqprev] and self[sqprev][track] and self[sqprev][track].lin then
                        
                        ligtoiter=self[sqprev][track].lin
                        
                end


            if ligtoiter and type(ligtoiter)=="number" then
                for lgprev=ligtoiter,1,-1 do
                        prev.diff=prev.diff+1
                        if self[sqprev] and self[sqprev][track][lgprev] and self[sqprev][track][lgprev][1] and self[sqprev][track][lgprev][1]==math.abs(p1) then
                               
                        prev.seq=sqprev
                        prev.lig=lgprev


                                break
                        end
                end
                if prev.seq then
                break
                end
            end


        end

        nextone.diff=0
        
        for sqnext=seq,#self do
                local ligtoiter,lignb
                if sqnext==seq then ligtoiter=lig+1
                else ligtoiter=1
                end
                
                        
                      
                --if not self[sqnext][track].lin then self[sqnext][track].lin=self.initlin end
                lignb=self[sqnext].lin
--print("sqnext",sqnext)
--print("lignb",lignb)               
--print("ligtoiter",ligtoiter)
                if ligtoiter<=lignb then
                for lgnext=ligtoiter,lignb do
                    nextone.diff=nextone.diff+1    
                        if  self[sqnext] and self[sqnext][track] and self[sqnext][track][lgnext] and self[sqnext][track][lgnext][1] and math.abs(self[sqnext][track][lgnext][1])==p1 then
                        nextone.seq=sqnext
                        nextone.lig=lgnext
                        break
                        end
                end
                end
                if nextone.seq then
                break
                end
        end



if nextone.seq and nextone.lig then
        local pos 
        if self[seq][track][lig][3] and self[seq][track][lig][3]>0 then pos=1 else pos=-1 end
        nextone.diff=nextone.diff-self[seq][track][lig][2]+self[nextone.seq][track][nextone.lig][2]
        p3=math.abs(self[seq][track][lig][3])
        if p3 and p3>0.9999999 then  p3=nextone.diff*pos
        elseif p3==nil then p3=nextone.diff*pos
        end
end
--if p3==nil then p3=1.123 end



if prev.seq and prev.lig then
        local pos
        if self[prev.seq][track][prev.lig][3] and self[prev.seq][track][prev.lig][3]>0 then 
		pos=1 
	else 
		pos=-1 
	end
--print("prev diff",prev.diff)
--print("prev seq",prev.seq)
--print("track",track)
--print("prev lig",prev.lig)
--print(self[seq][track][lig][2],self[prev.seq][track][prev.lig][2])

        prev.diff=prev.diff+self[seq][track][lig][2]-self[prev.seq][track][prev.lig][2]
        prev.p3=math.abs(self[prev.seq][track][prev.lig][3])
        if prev.p3 and prev.p3>0.99999999 then prev.p3=prev.diff*pos
        elseif prev.p3==nil then prev.p3=prev.diff*pos
        end
end
return p3,prev

--end

end


















-- This function outputs all the datas contained in a session of the tracker - it will be output in csound format (f,t and i statements)

function miup:score_output(mainseq,maintempo,data_func)        

        local esp=string.char(32)
        local cr=string.char(10)





--local array_str=""



--[[
if array_data then
	
	--local arrinit=""
	if array_data[1] and array_data[1][1] then
	--arrinit=string.format("gkArr[][][] init %d,%d,%d %c",#array_data,#array_data[1],#array_data[1][1],10)
		
			local arrinit=""		
		for k,v in pairs(array_data) do
		if type(k)=="number" and v[1] then 
			arrinit=string.format("gkArr%d[][] init %d,%d %c",k,#v,#v[1],10)
			
			for kl,vl in pairs(v) do
			if type(kl)=="number" then
				arrinit=string.format("%s %cgkArr%d_%d[] fillarray",arrinit,10,k,kl-1)
				for kc, vc in pairs(vl) do
					if type(vc)=="number" then
						if kc>1 then
							arrinit=string.format("%s, %f",arrinit,vc)
						else
							arrinit=string.format("%s %f",arrinit,vc)
						end
					end
				end

				arrinit=string.format("%s %cgkArr%d setrow gkArr%d_%d,%d",arrinit,10,k,k,kl-1,kl-1)	
				--arrinit=string.format("%s %cgiArr[%d][%d]=giArr%d_%d",arrinit,10,k,kl-1,k,kl-1)
				--arrinit=string.format("%s %cgkArr[%d] setrow gkArr%d_%d,%d",arrinit,10,k-1,k,kl-1,kl-1)
	
			end
			end


		end
array_str=string.format("%s%c%s",array_str,10,arrinit)
		end	
	end

end


]]--









local genfunc=""
if data_func then
for k,v in pairs(data_func) do
        if type(k)=="number" then
                if data_func[k].functype=="plotdraw" then
                        local f_stat=string.format("f %d 0 %d -16",k,16385)
                        local last_point=0
                        local last_curve
                        for key,val in pairs(data_func[k]) do 
                                if type(key)=="number" and val and type(val)=="table" and #val==3 then

                                        if key==0 then
                                                f_stat=string.format("%s %s",f_stat,data_func[k][key][2])
                                                last_curve=data_func[k][key][3]
                                        elseif key>0 then
						if data_func[k][key][2]>data_func[k][key-1][2]
						then last_curve=last_curve*-1 end	
                                                f_stat=string.format("%s %s %s %s",f_stat,tostring((data_func[k][key][1]-last_point)*16384),tostring(last_curve),tostring(data_func[k][key][2]))
                                                last_point=data_func[k][key][1]
                                                last_curve=data_func[k][key][3]
                                                
                                                

                                                

                                        
                                        end
                        if key==data_func[k].data_size then break end
                                end


                        end
                        genfunc=string.format("%s %s%s",genfunc,cr,f_stat)

                elseif data_func[k].functype=="sample" then
                        local f_stat=string.format("f %d 0 0 1 %s 0 0 0",k,data_func[k].sample)
                        genfunc=string.format("%s %s%s",genfunc,cr,f_stat)
                                
                elseif data_func[k].functype=="waveform" then
                        local f_stat=string.format("f %d 0 16384 -9",k)
                                for key,val in pairs(data_func[k]) do 
                                        if data_func[k][key] and type(data_func[k][key])=="table" and #data_func[k][key]==3 then
                                                f_stat=string.format("%s %s %s %s",f_stat,tostring(data_func[k][key][1]),tostring(data_func[k][key][2]),tostring(data_func[k][key][3]))
                                        end
                                end
                                
                        genfunc=string.format("%s %s%s",genfunc,cr,f_stat)
		elseif data_func[k].functype=="gentable" then
			local f_stat=string.format("f %d 0 %d -2",k,data_func[k].ligne)
			for key,val in pairs(data_func[k]) do
				if type(key)=="number" then
					f_stat=string.format("%s %s",f_stat,tostring(data_func[k][key]))
				end
			end
			genfunc=string.format("%s %s%s",genfunc,cr,f_stat)



                end


        end
end
end



local instances={}
local sigs={}
local tempo="t"
local score=""
local toadd,tempotoadd=0,0
        

for ks,vs in pairs(maintempo) do
	for kloop=1,tonumber(mainseq[ks].loopnumber) do
		for kl,vl in pairs(maintempo[ks]) do
		if type(ks)=="number" and type(kl)=="number" then
			--print("DEBUG",ks,kl)
			if maintempo[ks][kl][1] and type(maintempo[ks][kl][1])=="number" then
				tempo=string.format("%s %d %d",tempo,kl+tempotoadd-1,maintempo[ks][kl][1])
				if maintempo[ks][kl][2] then
				tempo=string.format("%s %d %d",tempo,kl+tempotoadd-1,maintempo[ks][kl][2])
				end
			end
		end
		end
		tempotoadd=tempotoadd+mainseq[ks].lin
	end
end




if mainseq and maintempo then
 
  for ks,vs in pairs(mainseq) do
    if type(vs)=="table" then
     for kloop=1,tonumber(vs.loopnumber) do
                        
          for kt,vt in pairs(mainseq[ks]) do
             if type(vt)=="table" then
              for kl,vl in pairs(vt) do
                if type(kl)=="number" then
                           



                     if vl and type(vl)=="table" and vl[1]~=nil and type(vl[1])=="number" then
                                                
                      local iscore="i"
        	                     for kc,vc in pairs(vl) do
                	                if kc==2 then
                                	iscore=string.format("%s %s",iscore,tostring(vc+toadd+kl-1))
                        	        else
                                        iscore=string.format("%s %s",iscore,tostring(vc))
                                        end
                                                
                                     end
                                
                                        score=string.format("%s %s%s",score,cr,iscore)

                     end
                                end
                                end
                                end
                       	end
                        if mainseq[ks].lin then toadd=toadd+mainseq[ks].lin end
                end
                end
                end

        end




local instr_tempo={}
local addinst=0
local itempo_string=""
for is=1,#mainseq do
for iloop=1,mainseq[is].loopnumber do
        for il=0,mainseq[is].lin-1 do
                local myt="i 1 " .. il+addinst .. " 1 " .. il+1
                table.insert(instr_tempo,myt)
        end

addinst=addinst+mainseq[is].lin
end

end
for k,v in pairs(instr_tempo) do
        itempo_string=string.format("%s%c%s",itempo_string,10,v)

end


local masterscore="i 999 " .. " 0 " .. toadd




local cr=string.char(10)
local gm=string.char(34)



if tempo=="t" then tempo="t 0 60" end
score=string.format("%s%c%s%c%s",itempo_string,10,score,10,masterscore)



local wscore=io.open("csound/scores/sco_test.sco","w")
--wscore:write(array_str)
--wscore:write(string.char(10))
wscore:write(tempo)
wscore:write(string.char(10))
wscore:write(genfunc)
wscore:write(string.char(10))
wscore:write(score)
wscore:flush()
wscore:close()

return tempo,genfunc,score



end












return miup


















