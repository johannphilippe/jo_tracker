
local ffi=require("ffi")
local thispath	
if ffi.os=="Linux" then
	os.setlocale("en_US.utf8")
	local path_call=io.popen("pwd","r")
	thispath=path_call:read("*all")
	path_call:close()
	thispath=string.gsub(thispath,string.char(10),"")
	package.cpath=package.cpath .. ";/usr/lib/lua/5.1/?.so"
	package.path=package.path .. ";" .. thispath .. "/share/lua/?.lua"
	package.terrapath=package.terrapath .. ";" .. thispath .. "/share/terra/?.t"
	terralib.includepath=terralib.includepath .. ";/usr/local/include/csound"
        terralib.linklibrary("libcsound64.so")
elseif ffi.os=="Windows" then
	os.setlocale("en_US.utf8")
	local path_call=io.popen("cd","r")
	thispath=path_call:read("*all")
	path_call:close()
	thispath=string.gsub(thispath,string.char(10),"")
	package.cpath=package.cpath .. ";" .. thispath .. "/bin/Windows/?51.dll"
        package.cpath=string.gsub(package.cpath,string.char(92),"/")
	package.path=package.path .. thispath .. "/share/lua/?.lua"
        package.path=string.gsub(package.path,string.char(92),"/")
	package.terrapath=package.terrapath .. thispath .. "/share/terra/?.t"	
        package.terrapath=string.gsub(package.terrapath,string.char(92),"/")
	terralib.includepath=terralib.includepath .. ";C:/Program Files (x86)/Windows Kits/10/Include/10.0.10586.0/ucrt;C:/Program Files (x86)/Microsoft Visual Studio/2017/Community/VC/Tools/MSVC/14.15.26726/include;C:/Program Files/Csound6_x64/include/csound"
        terralib.includepath=string.gsub(terralib.includepath,string.char(92),"/") 
        terralib.linklibrary("C:/Program Files/Csound6_x64/bin/csound64.dll")
end


print(ffi.os,"\n",package.cpath,"\n",package.path,"\n",package.terrapath,"\n",terralib.includepath)


--local orc_path=thispath .. "/csound/orc/orc_test.orc"



local miup=require("miup_object")
local orc=miup:new()
orc.fpath=thispath .. "/csound/orc/mainOrc.orc"


local serpent=require("serpent")
--Variable du volume master (à revoir)



-- Data storage table
local myseq=miup:new()
local mytempo=miup:new()


local global_params=dofile(thispath .. "/share/config/config.lua")








---------------------------- TEST SECTION 
local scopL=miup:new()
scopL:scope(16)
scopL.sc.size="200x20"
scopL.sc.expand="no"



local scopR=miup:new()
scopR:scope(16)
scopR.sc.size=scopL.sc.size
scopR.sc.expand="no"







---------------------------------------











---------------- Number boxes
local seqnbr=miup:new()
seqnbr:nbx(1,1,false,0,1,1)

local tracknbr=miup:new()
tracknbr:nbx(1,1,false,0,1,1)


local lignbr=miup:new()
lignbr:nbx(8,1,false,0,1,1)

local loopnbr=miup:new()
loopnbr:nbx(1,1,false,0,1,1)

local grille=miup:new()
grille:nbx(0,0,false,0,1,1)


myseq:store(lignbr,{seqnbr,"lin"})
myseq:store(lignbr,{"lin"})
myseq:store(loopnbr,{seqnbr,"loopnumber"})
---- Here the main arrays built with iup.matrix function - the first is used to write "i" score - and the second to write "t" tempo score




print("TEMPO")
local tempo=miup:new()
tempo:matrix(2,lignbr.val,{mytempo,seqnbr})
tempo.postconnect={
	{"store",mytempo,{seqnbr}},
}



local tracks=miup:new()


-- The connection tables for the tracks object has a specific behavior : it passes the table to all 
--child which adapt it to its idx, and its "self" variable name. Example the second tracks postconnect 
--could be method tracks[2] format tracker {myself seqnbr 2} 

tracks.preconnect={
	{"method",tracks,"track_set_current","idx"},
--        {"method","self","fill_info",orc},
}


tracks.postconnect={

        {"store",myseq,{seqnbr,tracks}},
        {"method","self","format_tracker",{myseq,seqnbr,tracks,thispath}},
        {"method","self","matrecall",{myseq,seqnbr,tracks}},
        {"method","self","fill_info",orc},
	
}



tracks:mat_tracks(8,lignbr.val,{myseq,seqnbr})



------------- Those lines are useful to connect the callbacks of several objects together
--for exemple, the first line assigns the lignbr.val to the m.mat.numlin, (the value of the number box lignbr to the number of lines of the main array)
--The "postconnect" subtable is executed after the value of the number box has changed
--the "preconnect" is executed before the value changed (useful to store the value in a data structure for example)
--
--The val statement is used to pass a value of a number box to a parameter of another object 
--The method statement is used to execute a function from miup at a given time. It doesn't work with all the functions called miup:something, only with miup.something. 
--Only a few methods (miup:something) are accessible from those pre_postconnect facilities.  For example, it's possible to add a table {"store",...} to the postconeect table



lignbr.postconnect={
	{"method",tracks,"track_resize"},
        {"val",tempo.mat,'numlin'},
	{"store",myseq,lignbr,{seqnbr,"lin"}},
	{"store",mytempo,lignbr,{seqnbr,"lin"}},
        {"method",tempo,"resizemat"},
	{"method",tracks,"track_colorline",{grille}},
        {"method",tempo,"mat_colorline",{grille}},
}





grille.postconnect={
	{"store",myseq,grille,{seqnbr,"grille"}},
	{"method",tracks,"track_colorline"},
	{"method",tempo,"mat_colorline"}
}


loopnbr.postconnect={
        {"store",myseq,loopnbr,{seqnbr,"loopnumber"}},
}

---------------------------------------------------------
--Création dun data set pour stocker mes séquences, tracks, etc.

--ICI le main qui contient les informations principales (séquence en cours etc)
--local valinfo=miup:new()




--myseq:store(tracks.initlin,{"initlin"})
tempo:matsync(tracks)
tracks[1]:storemat({myseq,seqnbr,1})
tempo:storemat({mytempo,seqnbr})


seqnbr.preconnect={

        {"storemat",tempo,{mytempo,seqnbr}},
        {"store",myseq,loopnbr,{seqnbr,"loopnumber"}},
        {"method",seqnbr,"findmax"},
   
        }

tracknbr.preconnect={
        {"method",tracknbr,"findmax"},
}

seqnbr.postconnect={

        {"method",lignbr,"assign",{myseq,seqnbr,"lin"}},
        {"method",loopnbr,"assign",{myseq,seqnbr,"loopnumber"}},
        {"method",tempo,"matrecall",{mytempo,seqnbr}},
	{"method",tracks,"track_recall",{myseq,seqnbr,tracks}},
	{"method",grille,"assign",{myseq,seqnbr,"grille"}},
        {"storemat",tempo,{mytempo,seqnbr}},
        {"store",myseq,loopnbr,{seqnbr,"loopnumber"}},
}

tracknbr.postconnect={
	{"method",tracks,"mat_tracks_change"},
        {"method",lignbr,"assign",{myseq,seqnbr,"lin"}},
	{"method",tracks,"track_colorline",{grille}},
	{"store",myseq,tracknbr,{"tracknbr"}},
}









--------------Callbacks à faire ici 
--ci -dessus
--associer le changement de taille à lécriture dans la table taille[seqnbr][tracknbr]
--changement de track ou seq à un storage complet
--faire le recall de track ou seq

--tracknbr:connect({"method",m,"storeall",{tonumber{seqnbr.txt.value},tonumber{tracknbr.txt.value}}},{"method",m,"clearall"})

--seqnbr:connect({"method",m,"storeall",{tonumber{seqnbr.txt.value},tonumber{tracknbr.txt.value}}},{"method",m,"clearall"})
--tracknbr:connect({"method",m,"clearall"})


-------------------------------------------------------
---------------FUNC = number of the currently edited (or recalled) GEN function
local func=miup:new()
func:nbx(1,1,false,0,1,1)

----------------------- DATAFUNC = data table for GEN functions 
local datafunc=miup:new()



--------------All of this for editing waveforms with an array 
local waveclear=iup.button{title="Clear"}
local wavestore=iup.button{title="Store"}
local wave=miup:new()
local wavemat_size=miup:new()


wavemat_size:nbx(8,1,false,0,1,1)
wave:matrix(3,8,{datafunc,func})

wave.tog_fct=function(ncol,gen)
	wave.mat.numcol=ncol
	wave.mode=gen
end

wave.mode="GEN10"
wave.tg1,wave.tg2=iup.toggle{title="GEN10"},iup.toggle{title="GEN2"}
local w_hb=iup.hbox{wave.tg1,wave.tg2}
wave.radio=iup.radio{w_hb}

function wave.tg1:action()
	wave.tog_fct(3,"GEN10")
end

function wave.tg2:action()
	wave.tog_fct(1,"GEN2")
end






wavemat_size.postconnect={
        {"val",wave.mat,"numlin"},
        {"method",wave,"resizemat"},
}

function waveclear:action()
	wave:clearall(wave)
end


function wavestore:action()
	wave:waveform_store(datafunc,func)
end





------------ Here for editing the plot (drawing curves etc)
local ymin=miup:new()
ymin:nbx(0,-1,false,1,0.05,1)

local ymax=miup:new()
ymax:nbx(1,ymin.val,false,1,0.05,1)
ymin.max=ymax.val

local ystep=miup:new()
ystep:nbx(0.1,0.05,false,1,0.05,1)

local xstep=miup:new()
xstep:nbx(0.1,0.05,false,1,0.05,1)



local myplot=miup:new()
myplot:plotdraw(ymin.txt.value,ymax.txt.value,ystep.txt.value,xstep.txt.value)


myplot.store=iup.button{title="Store"}
myplot.clear=iup.button{title="Clear"}


function myplot.store:action()
	myplot:plotstore(datafunc,func)
end


function myplot.clear:action()
	myplot:plotclear()
end




------------------- This for browsing and visualizing samples - soundfiles (only .wav currently  available)
local samp_draw=miup:new()
samp_draw:sampleplot()



samp_draw.store=iup.button{title="Store"}
samp_draw.clearsamp=iup.button{title="Clear"}
samp_draw.browsesamp=iup.button{title="Browse"}


function samp_draw.browsesamp:action()
	samp_draw:browse("samp")
end

function samp_draw.clearsamp:action()
	samp_draw.spclear(samp_draw)
end

function samp_draw.store:action()
	samp_draw:samplestore(datafunc,func)
end






-- A revoir
funcparse=miup:new()
funcparse.plotdraw=myplot
funcparse.waveform=wave
funcparse.gentable=wave
funcparse.sample=samp_draw



----------------- Here all the data useful to edit GEN functions are connected 
--For example the y_min value stored in the datafunc[func.val].y_min is recalled in ymin.val number box, so that the plot is redimensionned before recalling datas 
--The recall is done with the function "funcrecall"  just after resizing all objects.
func.preconnect={
        {"method",myplot,"plotclear"},
        {"method",samp_draw,"spclear"},
        {"method",wave,"clearall"},
}

func.postconnect={
        {"method",ymin,"assign",{datafunc,func,"y_min"}},
        {"method",ymax,"assign",{datafunc,func,"y_max"}},
        {"method",ystep,"assign",{datafunc,func,"ygrid"}},
        {"method",xstep,"assign",{datafunc,func,"xgrid"}},
        {"method",wavemat_size,"assign",{datafunc,func,"ligne"}},
        {"method",datafunc,"funcrecall",{datafunc,func}}, 

}

ymin.postconnect={
        {"val",myplot.plot,"AXS_YMIN"},
        {"val",ymax,"min"},
        {"method",myplot,"plotredraw"},
}

ymax.postconnect={
        {"val",myplot.plot,"AXS_YMAX"},
        {"val",ymin,"max"},
        {"method",myplot,"plotredraw"},
}

ystep.postconnect={
        {"val",myplot.plot,"AXS_YTICKMAJORSPAN"},
        {"method",myplot,"plotredraw"},
}

xstep.postconnect={
        {"val",myplot.plot,"AXS_XTICKMAJORSPAN"},
        {"method",myplot,"plotredraw"},
}














-----------------MONITOR / MIX TABLE























-------------------------------------------------------------------------------
--                      SAVE AND LOAD ITEMS FOR MENU
---------------------------------------------------------------------------------
--
--
------------SAVE


local saveas=miup:new()
saveas.menuitem=iup.item{image="IUP_FileSave",title="SAVE AS"}


local save=miup:new()
save.menuitem=iup.item{image="IUP_FileSave",title="SAVE"}

--fonction générique pour sauvegarder le contenu
local function save_content()

	local jump=string.format("%c%c",10,10)

		print(saveas.fpath)
		local jo=io.open(saveas.fpath,"w")
		jo:write("ORCHESTRA_PATH:" .. orc.fpath .. "_ENDPATH" .. jump)
		jo:write("MAINSCORE" .. jump)
		jo:write(serpent.dump(myseq,{indent='   ',numformat="%g",name="myseq"}) .. jump)
		jo:write("ENDSCORE")
		jo:write(jump)
		jo:write("MAINTEMPO" .. jump)
		jo:write(serpent.dump(mytempo,{indent='   ',numformat="%g",name="mytempo"}) .. jump)
		jo:write("ENDTEMPO")
		jo:write(jump)
		jo:write("DATAFUNC" .. jump)
		jo:write(serpent.dump(datafunc,{indent='   ',numformat="%g",name="datafunc"}) .. jump)
		jo:write("ENDFUNC")
		jo:flush()
		jo:close()

end


--function save as si pas de fichier path 
function saveas.menuitem:action()
	--local alarm=1
	saveas:browse()
	
	if saveas.browsestat~=-1 then

		local extension=string.match(saveas.fpath,"%..*")
		if not extension then 
			saveas.fpath=saveas.fpath .. ".jo"
		else
			saveas.fpath=string.gsub(saveas.fpath,"%..*",".jo")
		end
		save_content()
--		saveas.fpath=nil
--		saveas.browsestat=nil

	end

end






--function save normal, automatic si déjà path
function save.menuitem:action()
	if saveas.browsestat and saveas.browsestat~=-1 then
		save_content()	

	else
		saveas.menuitem:action()

	end

end






----------------- LOAD

local loadproject=miup:new()



loadproject.menuitem=iup.item{image="IUP_FileOpen",title="LOAD"}

function loadproject.menuitem:action()

	loadproject:browse()
	
	if loadproject.browsestat~=-1 then


		myseq:clear_content()
		mytempo:clear_content()
		datafunc:clear_content()


		tracks.track_clear(tracks)
		tempo.clearall(tempo)

		local jo=io.open(loadproject.fpath,"r")
		local lfile=jo:read("*all")
		local pidb,orcp,pide=string.match(lfile,"(ORCHESTRA_PATH:)(.*)(_ENDPATH)")
		local midb,myseq_load,mide=string.match(lfile,"(MAINSCORE)(.*)(ENDSCORE)")
		local tidb,mytempo_load,tide=string.match(lfile,"(MAINTEMPO)(.*)(ENDTEMPO)")
		local didb,datafunc_load,dide=string.match(lfile,"(DATAFUNC)(.*)(ENDFUNC)")

		local ok,tempseq,temptempo,tempfunc

		print("orchestra_path --->  ",orcp)	
		ok,tempseq=serpent.load(myseq_load,{safe=true})
		ok,temptempo=serpent.load(mytempo_load,{safe=true})
		ok,tempfunc=serpent.load(datafunc_load,{safe=true})


		tracknbr.assign(tracknbr,{tempseq,"tracknbr"})
		
		myseq:table_copy(tempseq)
		mytempo:table_copy(temptempo)
		datafunc:table_copy(tempfunc)	

		
		seqnbr:nbx_connect("postconnect")
		func:nbx_connect("postconnect")
		
		saveas.browsestat,saveas.fpath=loadproject.browsestat,loadproject.fpath
		loadproject.browsestat,loadproject.fpath=nil,nil


		-- To avoid plot bug  graphic
		iup.Redraw(myplot.plot,1)

	end

end























local user_data,ThreadID



--------------------------View METER
local vmeterL,vmeterR=miup:new(),miup:new()
vmeterL:viewmeter(0,1)
vmeterR:viewmeter(0,1)
vmeterL.meter.value=0
vmeterR.meter.value=0


vmeterL.meter.size="200x12"
vmeterR.meter.size=vmeterL.meter.size








--- The master volume slider
local master=miup:new()
master:slider(0.7,0,1,"horizontal")
master.sl.size=vmeterL.meter.size

------------------------- This function is used to refresh the audio connections (output control channel) so that it matches the right user_data
---------------------- All the output connections from Miup to csound (slider, nbx etc) must be define here if they connect to the user_data
-- In this case, my master.val is connected to user_data.mastval (where the control channel go read the master vol value)
local function audio_connect()
master.postconnect={
        {"terra_connect",user_data,"mastval"},
}
end






------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------              CSOUND INIT          ----------------------------------------------
------------------------------------------------------------------------------------------------------------------------------
--user_data=cs_init()




-------------LOAD ORCHESTRA

orc.menuitem=iup.item{title="Load Orchestra"}



function orc.menuitem:action()

	local orcfile
	orc:browse()
	
	if orc.browsestat~=-1 then
	        orc.fpath=orc.fpath
	end


end

















local user_data




---------------------------------- CALLBACKS FUNCTIONS TO REFRESH GUI ------------------------
----------------------------------------------------------------------------------------------
-- Struct containint callback buffers





local mark,oldmark
local synctab={seq=1,loop=1,lin=0}


local function track_callback_init()
synctab={seq=1,loop=1,lin=0}

	for k,v in pairs(tracks) do
		if type(k)=="number" and tracks[k].mat then
			tracks[k].mat.markmode="LIN"
		end
	end

	seqnbr.assign(seqnbr,{synctab.seq})
end




local function track_callback_end()
	if type(mark)=="string" then
		for k,v in pairs(tracks) do
			if type(k)=="number" and tracks[k].mat then
				tracks[k].mat[mark]=0
				tracks[k].mat.redraw="yes"
				tracks[k].mat.markmode="CELL"
			end
		end
	end
return 1

end







local function track_callback(val)
	synctab.lin=val

	if type(mark)=="string" then oldmark=mark end

	mark=string.format("MARK%d:0",val)
	for k,v in pairs(tracks) do 
		if type(k)=="number" and tracks[k].mat then
			if type(oldmark)=="string" then
				tracks[k].mat[oldmark]=0
			end
				tracks[k].mat[mark]=1
				tracks[k].mat.redraw="yes"
		end
	end

	if synctab.lin==1 and synctab.seq>1 then 
		if synctab.loop<myseq[synctab.seq].loop then
			synctab.loop=synctab.loop+1
		elseif synctab.loop==myseq[synctab.seq].loop then
			synctab.seq=synctab.seq+1
			synctab.loop=1
			seqnbr.assign(seqnbr,{synctab.seq})
		end
	 
	end



end



local function StopIdle(hdt)
	iup.SetIdle(nil)
	vmeterL.meter.value=0
	vmeterR.meter.value=0	
--        scopL:scope_init()
  --      scopR:scope_init()	
        track_callback_end()
	hdt=nil
	collectgarbage("collect")
print("STOP IDLE END")

end


local function ev_callback(idd,val)
	local idx=ffi.string(idd)
	if idx=="level_L" then
		vmeterL.meter.value=math.abs(val)	
	elseif idx=="level_R" then
		vmeterR.meter.value=math.abs(val)
      --  elseif idx=="scopeL" then
      --          scopL:scope_write(val)
      --  elseif idx=="scopeR" then
      --          scopR:scope_write(val)
      --  elseif idx=="scopeT" then
      --          scopT:scope_write(val)
       	elseif idx=="T_sync" then
		track_callback(val)
	end

end


local evaluate_callback=terralib.cast({&int8,double} ->{},ev_callback)
local StopIdle_terra=terralib.cast({&opaque}->{},StopIdle)


local terra check_buf(dataptr:&opaque)
	var hd:&bufs=[&bufs](dataptr)
		csnd.csoundLockMutexNoWait(hd.mtx)
			csnd.csoundReadCircularBuffer(nil,hd.gui_buf,&hd.gui_read,1)	
		        evaluate_callback(hd.gui_read.idx,@hd.gui_read.val)
		csnd.csoundUnlockMutex(hd.mtx)
	if perf_stat==0 then
		StopIdle_terra(dataptr)	
	end

end





















--------------------- Play/Stop button ----------------------------
-------------------------------------------------------------
local play=miup:new()
play.playbutton=iup.button{image="IUP_MediaPlay",title="Play",canfocus="no"}
play.playbutton.size="60x13"

local stop=miup:new()
stop.stopbutton=iup.button{image="IUP_MediaStop",title="Stop",canfocus="no"}
stop.stopbutton.size=play.playbutton.size


local rec=miup:new()
rec.recbutton=iup.button{image="IUP_MediaRecord",title="Rec",canfocus="no"}
rec.recbutton.size="60x13"
---------- PLAY ACTION -------------

local function play_perf(stat)

audio_connect()
print("playbutton STARTED")
-----LOAD ORCHESTRA AS STRING
	print(orc.fpath)
	local op=io.open(orc.fpath,"r")
	local orcstr=op:read("*all")
	op:close()
	
	local mastpath=thispath .. "/csound/orc/instr_master/master_realtime.orc"
	local mp=io.open(mastpath,"r")
	local mast_orc=mp:read("*all")
	mp:close()



if stat=="rec_stereo" and rec.browsestat~=-1 then
	local mystr=string.format("fout %c%s%c,8,aL,aR",34,rec.fpath,34)
	mast_orc=string.gsub(mast_orc,";rec",mystr)
end        
	
	print("READ OP SUCCEED")
	tracks.track_store_all(tracks)
	tempo:storemat({mytempo,seqnbr})
	myseq:store(loopnbr,{seqnbr,"loopnumber"})




	local forc=orcstr .. string.char(10) .. mast_orc

	print(forc)	
	local tmpo,genfc,sco=myseq:score_output(myseq,mytempo,datafunc)

	

	local fscore=string.format("%c%s%c%s%c%s",10,tmpo,10,genfc,10,sco)
	print(fscore)	
	--print(orc,string.char(10),"--------------------------------------------------------------------",string.char(10),fscore)

	local hdata,stat

	hdata,stat=cs_start_perf(forc,fscore)
	local perf=perf_stat:get()

local	function myidle_cb()
		if hdata and hdata~=nil then
			check_buf(hdata)
		end
	end


if perf==1 then
	track_callback_init()
	iup.SetIdle(myidle_cb)

end



end

function play.playbutton:action()
	play_perf("play")
end

--------------------- Stop button ----------------------------
-------------------------------------------------------------


function stop.stopbutton:action()
	--print("STOP")
	--print("IDLE RELEASED")
	cs_stop()

end







function rec.recbutton:action()
	rec:browse()
	
	if rec.browsestat~=-1 then

		local extension=string.match(rec.fpath,"%..*")
		if not extension then 
			rec.fpath=rec.fpath .. ".wav"
		else
			rec.fpath=string.gsub(rec.fpath,"%..*",".wav")
		end



		play_perf("rec_stereo")
	end


end






------------------------------------RENDER FUNCTIONS ----------------------------------------
---------------------------------------------------------------------------------------------
local render_stereo=miup:new()
render_stereo.menuitem=iup.item{title="Render Stereo"}

function render_stereo.menuitem:action()

	render_stereo:browse()

	if render_stereo.browsestat~=-1 then

		local extension=string.match(render_stereo.fpath,"%..*")
		if not extension then 
			render_stereo.fpath=render_stereo.fpath .. ".wav"
		else
			render_stereo.fpath=string.gsub(render_stereo.fpath,"%..*",".wav")
		end
			render_stereo.fpath="--output=" .. render_stereo.fpath

		local op=io.open(orc.fpath,"r")
		local orc=op:read("*all")
		op:close()

		local mastpath=thispath .. "/csound/orc/instr_master/master_offline.orc"
		local mp=io.open(mastpath,"r")
		local mast_orc=mp:read("*all")
		mp:close()

		local forc=orc .. string.char(10) .. mast_orc


		local tmpo,genfc,sco=myseq:score_output(myseq,mytempo,datafunc)
		local fscore=string.format("%c%s%c%s%c%s",10,tmpo,10,genfc,10,sco)
	
		render(forc,fscore,render_stereo.fpath,"--format=wav:24bit")

	end

render_stereo.fpath=nil
render_stereo.browsestat=nil
end










----------------------------------EXPORT AS CSD --------------------------------------------
--------------------------------------------------------------------------------------------
local export_csd=miup:new()
export_csd.menuitem=iup.item{title="Export .CSD"}

function export_csd.menuitem:action()
		
	export_csd:browse()

	if export_csd.browsestat~=-1 then

		local extension=string.match(export_csd.fpath,"%..*")
		if not extension then 
			export_csd.fpath=export_csd.fpath .. ".csd"
		else
			export_csd.fpath=string.gsub(export_csd.fpath,"%..*",".csd")
		end
	

	local op=io.open(orc.fpath,"r")
	local orc=op:read("*all")
	op:close()
	
	local mastpath=thispath .. "/csound/orc/instr_master/master_offline.orc"
	local mp=io.open(mastpath,"r")
	local mast=mp:read("*all")
	mp:close()
	
	local forc=orc .. string.char(10) .. mast
	
	local tmpo,genfc,sco=myseq:score_output(myseq,mytempo,datafunc)
	local fscore=string.format("%c%s%c%s%c%s",10,tmpo,10,genfc,10,sco)

	local cr=string.char(10)
	local csd_string=string.format("%s%c%s%c","<CsoundSynthesizer>",10,"<CsOptions>",10)
	csd_string=string.format("%s%c%s%c%s%c%s",csd_string,10,"-odac",10,"</CsOptions>",10,"<CsInstruments>")
	csd_string=string.format("%s%c%s%c%s%c%s",csd_string,10,forc,10,"</CsInstruments>",10,"<CsScore>")
	csd_string=string.format("%s%c%s%c%s%c%s",csd_string,10,fscore,10,"</CsScore>",10,"</CsoundSynthesizer>")

	csd_string=string.gsub(csd_string,string.char(13),"")
	local csd=io.open(export_csd.fpath,"w")
	csd:write(csd_string)
	csd:flush()
	csd:close()
	
	end

export_csd.browsestat=nil
export_csd.fpath=nil


end







--
----------------------- ICI les fonctions pour les boutons de clear - fait ici et non dans la bibliotheque miup car cela ne concerne que le djonoise
---------------------Reste à faire un clear tempo et aussi un "new project" qui détriut tout (seqmax, trackmax, functions etc)




local reset_tracker_button=iup.item{image="IUP_EditErase",title="Reset Tracker"}

function reset_tracker_button:action()
	tracks.track_clear(tracks)
	tempo.clearall(tempo)
	wave.clearall(wave)	
	myplot:plotclear()
	samp_draw.spclear(samp_draw)


	myseq:clear_content()
	datafunc:clear_content()
	mytempo:clear_content()	
	seqnbr:nbx_connect("postconnect")

	saveas.browsestat,saveas.fpath=nil,nil

end


local clearall_button=iup.item{image="IUP_EditErase",title="Tracker Clear All"}
function clearall_button:action()
	
	tracks.track_clear(tracks)
	tempo.clearall(tempo)

	myseq:clear_content()
	mytempo:clear_content()
	seqnbr:nbx_connect("postconnect")
end


local clearseq_button=iup.item{image="IUP_EditErase",title="Tracker Clear Seq"}
function clearseq_button:action()

	local seq_number=tonumber(seqnbr.txt.value)
	tempo.clearall(tempo)
	tracks.track_clear(tracks)
	myseq:mydestroy({seq_number})
	mytempo:mydestroy({seq_number})
	seqnbr:nbx_connect("postconnect")

end


local cleartrack_button=iup.item{image="IUP_EditErase",title="Tracker Clear Track"}
function cleartrack_button:action()
	local track_number=tracks.val
	tracks.track_clear(tracks,{track_number})

		for k,v in pairs(myseq) do
			if type(k)=="number" then
		        	myseq:mydestroy({k,track_number})
			end
		end

end



















----------------------------------------------------------------------
----------------------------------- Parametres ------------------------
local copyseq_from=miup:new()
local copyseq_to=miup:new()
copyseq_from:nbx(1,1,false,0,1,1)
copyseq_to:nbx(1,1,false,0,1,1)

local copyseq_button=iup.button{title="Copy Seq"}

function copyseq_button:action()

	local seq_number=tonumber(copyseq_to.txt.value)
	local seq_from=tonumber(copyseq_from.txt.value)
	local curseq=tonumber(seqnbr.txt.value)

	if seq_number==curseq then	
		tempo.clearall(tempo)
		tracks.track_clear(tracks)
	end


	myseq:mydestroy({seq_number})
	mytempo:mydestroy({seq_number})
	myseq[seq_number]=miup:new()
	mytempo[seq_number]=miup:new()
	myseq[seq_number]:table_copy(myseq[seq_from])	
	mytempo[seq_number]:table_copy(mytempo[seq_from])
	seqnbr:nbx_connect("postconnect")


end

local copyseq_hbx=iup.hbox{copyseq_from.nbx,copyseq_to.nbx,copyseq_button}
local copyseq_dlg=iup.dialog{copyseq_hbx;title="Copy seq",expand="no"}
local copyseq_item=iup.item{title="Copy seq"}

function copyseq_item:action()
	copyseq_dlg:show()
end





local param_click=iup.item{title="Track Param"}

function param_click:action()
	local parampath=thispath .. "/share/config/config.lua"
	
	local params=dofile(parampath)
	local visible_col_nbx=miup:new()
	visible_col_nbx:nbx(params.default_col_visible,1,false,0,1,1)
	visible_col_nbx.postconnect={
			{"method",tracks,"track_col_visible",visible_col_nbx},
			{"method",visible_col_nbx,"serialize",{parampath,params,global_params}}
	}	

	local auto_calc=iup.toggle{title="Auto Calculate p3"}
	if params.auto_calculate_dur==1 then auto_calc.value="ON" else auto_calc.value="OFF" end
	function auto_calc:action(state)		
		params.auto_calculate_dur=state
		miup.serialize(miup,{parampath,params})
		global_params=dofile(parampath)
	end

	local paramvbx=iup.vbox{visible_col_nbx.nbx,auto_calc}
	local param_dlg=iup.dialog{paramvbx;title="Parameters";expand="yes"}

	param_dlg:show()
end

















---------------------------Menus box --------------------------
-----------------------------------------------------------
local file_items=iup.menu{saveas.menuitem,save.menuitem,loadproject.menuitem,iup.separator{}, orc.menuitem,iup.separator{},export_csd.menuitem,render_stereo.menuitem}
local file=iup.submenu{file_items;title="Files"}

local action_items=iup.menu{reset_tracker_button,iup.separator{},clearall_button,clearseq_button,cleartrack_button,iup.separator{},copyseq_item}
local action=iup.submenu{action_items;title="Actions"}


local param_items=iup.menu{param_click}
local param=iup.submenu{param_items;title="Parameters"}





local menu=iup.menu{file,action,param}







-------------------------------------------------------------------------------
--BOXES AND DIALOGS
------------------------------------------------------------------------------



local infobox=iup.frame{
        iup.hbox{
		iup.frame{seqnbr.nbx;title="SEQNBR"},
		iup.frame{tracknbr.nbx,title="TRACKNBR"},
		iup.frame{lignbr.nbx;title="LIGNBR"},
		iup.frame{loopnbr.nbx;title="loop"},
		iup.frame{grille.nbx;title="grille"},
        };title="INFO"
}









local matbox=iup.hbox{
	iup.scrollbox{tracks.conteneur;scrollbar="yes",expand="yes",expandchildren="yes",size="1150x",maxsize="1150x";BACKGROUND="80 0 0"},
	iup.vbox{
		iup.frame{tempo.clearbut;title="Tempo",expand="yes",size="100x26.5"},
		tempo.mat;
		ALIGNMENT="ARIGHT",maxsize="200x",expandchildren="no",expand="yes"}

;title="Tempo",expandchildren="vertical",scrollbar="vertical",maxsize="200x"}





local playbox=iup.frame{
        iup.hbox{
                iup.vbox{play.playbutton,iup.fill{},rec.recbutton,iup.fill{},stop.stopbutton},
                iup.vbox{vmeterL.meter,master.sl,vmeterR.meter}};
        title="Play"
}
                

local scopbox=iup.frame{
        iup.vbox{scopL.sc,scopR.sc}
;title="SCOPE"}


local main_tab=iup.frame{
	iup.vbox{
                iup.hbox{playbox,infobox,scopbox;expand="YES"},
                matbox;expandchildren="HORIZONTAL"}}
main_tab.tabtitle="main"





myplot.plot.size="700x150"
local plot_box=iup.frame{
        iup.vbox{
                iup.hbox{func.nbx,myplot.clear,myplot.store,ymin.nbx,ymax.nbx,ystep.nbx,xstep.nbx},
                iup.hbox{myplot.plot,expandchildren="horizontal"}
        };title="plot"
}

local sample_box=iup.frame{
        iup.vbox{
                iup.hbox{samp_draw.browsesamp,samp_draw.store,samp_draw.clearsamp},
                iup.hbox{samp_draw.samp,expandchildren="horizontal",maxsize="FULLx200",SCROLLBAR="VERTICAL"}
        };title="Sample"
}

        local waveform=iup.frame{
                iup.vbox{
                        iup.hbox{wavemat_size.nbx,waveclear,wavestore,wave.radio},
                        wave.mat,maxsize="300x",SCROLLBAR="VERTICAL"
                };title="waveform"                
        }



local  func_tab=iup.hbox{
        iup.fill{},
        iup.vbox{plot_box,sample_box},
        waveform,
        iup.fill{}
}

func_tab.tabtitle="plottab"


local multi=miup:new()
multi:multislider(8,0,0,1)
multi.multislider.size="10x10"


local test_tab=iup.hbox{
        iup.fill{},
        iup.vbox{multi.multislider,expandchildren="both"},
        iup.fill{},
}
test_tab.tabtitle="test tab"







-----------------------------------------------------------------------------------------------
-------------------------------------TABS-----------------------------------------------------
local tabs=iup.tabs{main_tab,func_tab,test_tab,expandchildren="both";BACKGROUND="80 0 0"}
function tabs:tabchange_cb(new_tab,old_tab)
	if new_tab==main_tab then
		print("YOUPI")
		scopL.sc.redraw="yes"
		scopR.sc.redraw="yes"
	elseif new_tab==func_tab then


	end

end


local dlg=iup.dialog{iup.hbox{tabs,expandchildren="both"};title="JO_TRACKER",size="FULLxFULL",expandchildren="both",BACKGROUND="80 0 0",menu=menu}


--if ffi.os=="Linux" then dlg.placement="FULL" end
dlg:showxy(iup.CENTER, iup.CENTER)



--Responsive dialog resizing here -- 
--TODO : resizing all tabs (tab2 also)
function dlg:resize_cb(w,h)
	local str=""
	local nb=(w-160)
	str=nb.."x"
	matbox[1].maxsize=str
end





function gbutton(but, press, x, y, status)
	
--  print("globalcbk",but, press, x, y, status)

end


function gkey(key,stat)

	--print("globalkey",key,stat)
	if stat==0 then
		local perf=perf_stat:get()
	
		if key==32 then
			if perf==0 then
				play.playbutton:action()
			end
		elseif key==65307 then
			if perf==1 then
				stop.stopbutton:action()
			end
		elseif key==536870991 then
			loadproject.menuitem:action()
		elseif key==536870995 then
			save.menuitem:action()
		end
	end

key,stat=nil
end
 

iup.SetGlobal("INPUTCALLBACKS", "Yes")

--iup.SetGlobalCallback("GLOBALBUTTON_CB", gbutton)
iup.SetGlobalCallback("GLOBALKEYPRESS_CB",gkey)
-- FOR NORMAL COLOR BACKGROUND = 182 182 182




if iup.MainLoopLevel()==0 then
	iup.MainLoop()

end





















--[[
--
45 26 28 255 bordeaux
--
--
45 26 28 255 bordeaux + sombre

--
--
-79 53 52 gris metal-
--
--46 38 38 plus foncé
--
--
--19 16 16 encore plus foncé
--
--
--
--15 17 47 bleu ocean8N9YY-BKJ97-QFPFY-K6HFG-XQBQB
--
--
--
--
--
--
--]]--
