TODO
=====
======================================
AUDIO
======================================

--Multi_track offline render

--REC button - for at least stereo online recording
==done
--REC button for multitrack recording (using fout ou anything else)

--Create a method to assign objects to callbacks (or the opposite)





=====================================
INTERFACE 
=====================================

IMPORTANT
-----------
*libsndfile - for waveforms representations - including multiple zoom (rescale)
and probably a peak file (like reapeaks) 
https://www.reaper.fm/sdk/reapeaks.txt

Another solution would be to use csound in non-realtime mode with foutk (downsamped) and with a low-cost bitrate (32 float for example) to output a binary file, 
and create a lua wrapper to easily read those binaries peak files.


*GEN08 and GENquadbezier for the plot 


-EDITING SEQ DIALOG : un dialog ou un objet graphique pour reconfigurer les séquences entre elles, en copier une etc...


-bouton rotatif



--- BIG THINKABOUT : Make a scripting "interpretor" with pattern matching wich could allow to precise some parameterrs
like rescaling a function, could looklike 
scalefunc(10,-1,1)  //scale action,funcnbr, between -1 and 1
or
mtof(70.875)
or some other utilities i could think about. That would make more easy the rewriting of functions.

also, for gen2 : 
offset(50,21) // would give an offset of 21 to the func nbr 50 if it is a GEN2 func
offset could also work for gen 16, gen8 and quadbezier.



For scalefunc, lua would generate a genfunction with a particular number (reserved ones, after 1000 for examples)
That would only work with functions generated with a ymin and ymax. 

For that, the gen_format to csound format method must be public


Also, it could be complemented with a variable system, where i could define something like tone=70
so i could use : mtof(tone+8)

How to implement that without breaking lua expression evaluation module. Like 5+8 etc. 
It needs to be a real langage with a parser. Terra might be a good stuff for that.






SECONDAIRE
----------
--Implement relative path (in a script looking for the root folder and configuring includepathes)


To think about : 
--Externalizing data methods in a data class
--Separate particular methods in separat classes, so that creating a new object doesn't need to get public access to every methods, but only the one required (see
about inheritence, so that any class could inherit data class).



-MATRIX make the copy_buffer inheritable from metaclass
==DONE


-Track system  : BIG WORK 
	Change the track system to a 3 col matrix, that can be "+" to show entire score of the track
	The goal is to visualise multiple tracks at the same time
==DONE

-Make the "tempo" track a bit belower - so that it matches all the tracks.
==DONE

-Jo_tracker findnextprev : two modes, one that detects only the same instance, 
and the other one detecting everything. That could be set in a menu "preferences".
-Same function : more precise with P2

-Configuration file loaded at startup with all the config and can be changed
This config file also must contain defaults params, to reset de config.



-Writing the orcpath in the .jo save file 
==DONE


-- BIG WORK 
-- CHANGE THE CONNECT SYSTEM so that it matches only regular method (with "."  rather than ":"). So the syntax doesn't the "method" argument. like this : 
truc.postconnect={
	{truc,"storemat",{data,idx}},
}
For that i need to replace all method with ":" by "." methods (only for action methods, not for objects)

Still useful : "val", "storemat","store",etc. The val is maybe useful.


-- MATRIX 
Make all the utilities of the matrix (numberbox for cols, ligs,clearbox, storebox etc available inside the object, so that i don't need to create this in the main)



--Multislider : 
Make a step argument (step 0.5, or step1 for example )
Change the mouse callback so that it can be like in max (?)



-- Plot
Make a way to rescale automatically the y tick majorspan when making the plot ymax higher.
Or 
Make the ystep number box pointing directly to the number of gridsteps so that it refreshes everytime when the size changes


--GEN MATRIX : 
Recall the number of points on the numberbox
==DONE

--Numberbox
Make a way to not process the callback until enter key is pressed for example
(only when typing numbers)
==DONE
