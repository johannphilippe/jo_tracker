instr 999
	kmast init 1
	kold init 1
	kcount init 1


	kmast chnget "Master"
	kmast lineto kmast, 0.01
	amast=a(kmast)


	aL=amast*gaL
	aR=amast*gaR

	kaL rms aL,10
	kaR rms aR,10
        kscL=k(aL)
        kscR=k(aR)
        ilim init 1024

ar lfo 0.5,10,0
kres=k(ar)

	if (kcount==ksmps) then
	        outvalue "level_L",kaL
	        outvalue "level_R",kaR
                outvalue "scopeL",kscL
                outvalue "scopeR",kscR
	elseif (kcount>=ilim) then
	        kcount=0
	endif


	kcount = kcount+ksmps
	outs aL,aR

;rec

	clear gaL,gaR
endin
