<CsoundSynthesizer>
<CsOptions>
-odac

</CsOptions>


<CsInstruments>
sr = 44100
ksmps = 64
nchnls = 2
0dbfs = 1



instr 1 
kamp=p4
kcps=p5

ares oscili kamp,kcps

outs ares,ares

endin



</CsInstruments>




<CsScore>
i 1.1 0 10 1 500
i 1.2 2 8 1 800


</CsScore>



</CsoundSynthesizer>
