--[[
This is an example on how to acces to Csound C api using Terra.
Terra is a low-level system programming language that is embedded in and meta-programmed by the Lua programming language.
This example shows how to initialize, create, and run a simple instance of csound through the API.
This test was done on Windows 10 x64, using Csound 6.10 x64 (pre-build), Terra binaries distribution  (march 2016), and Visual Studio 2017 packages as C standard library. 
]]--





-- You can change this path for your configuration. If Terra doesnt automatically find the C standard library, you need to include its path. And you also need to include the csound.h to your include path. 
terralib.includepath="C:/Program Files (x86)/Windows Kits/10/Include/10.0.10586.0/ucrt;C:/Program Files (x86)/Microsoft Visual Studio/2017/Community/VC/Tools/MSVC/14.15.26726/include;C:/Program Files/Csound6_x64/include/csound"

--Here I link the dynamic library 
terralib.linklibrary("C:/Program Files/Csound6_x64/bin/csound64.dll")


--And include the C headers youll need - those lua tables containing headers will be called from terra functions with the same syntax than in Lua, e.g : 
-- c.printf("Hello World")
local c=terralib.includec("stdio.h")
local csound=terralib.includec("csound.h")



--Terra functions can be wrote like this - arguments need to be statically typed, like return values e.g : 
--terra main(a:int,b:bool) : int  
--var a:int=2
-- return a
--end
--


terra create()
        csound.csoundInitialize(0)
        
        var ptr : &opaque
        var csnd=csound.csoundCreate(ptr)

        csound.csoundSetOption(csnd,'--output=dac')
        csound.csoundSetOption(csnd,'--format=float')

        var result = csound.csoundCompileOrc(csnd, [[
        sr = 48000
        ksmps = 100
        nchnls = 2
        0dbfs=1

        instr 1 
        kamp=p4
        kcps=p5
        aout oscili kamp,kcps

        outs aout,aout
        endin       
]])


        csound.csoundReadScore(csnd,"i 1 0 3 0.2 150")

        result=csound.csoundStart(csnd)
        csound.csoundPerform(csnd)
end
create()
