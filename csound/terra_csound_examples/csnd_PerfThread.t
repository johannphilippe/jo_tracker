--[[
This example is a simple translation (simplified) of the Csound C Api example provided in the Floss Manual here : 
http://write.flossmanuals.net/csound/a-the-csound-api/


]]--




terralib.includepath="C:/Program Files (x86)/Windows Kits/10/Include/10.0.10586.0/ucrt;C:/Program Files (x86)/Microsoft Visual Studio/2017/Community/VC/Tools/MSVC/14.15.26726/include;C:/Program Files/Csound6_x64/include/csound"



terralib.linklibrary("C:/Program Files/Csound6_x64/bin/csound64.dll")


local c=terralib.includec("stdio.h")
local csnd=terralib.includec("csound.h")
local std=terralib.includec("stdlib.h")



-- In Terra its possible to define struct as in C - here this structure was necessary to parse data to the Perf_Routine
struct Udata {
result : int;
PERF_STATUS : int;
csound:&csnd.CSOUND_;
}


-- This Routine that  will be called from csoundCreateThread
terra Perf_Routine(dataptr : &opaque) : uint64

        var ud:&Udata=[&Udata](dataptr)
        csnd.csoundPerform(ud.csound)

end


terra main()


        var ptr:&opaque
        var ud:&Udata = [&Udata](std.malloc(sizeof(Udata)))
        ud.result=csnd.csoundInitialize(0)
        ud.csound=csnd.csoundCreate(ptr)

        csnd.csoundSetOption(ud.csound, '--output=dac')
        csnd.csoundSetOption(ud.csound, '--format=float')


        ud.result=csnd.csoundCompileOrc(ud.csound, [[
        sr = 48000
        ksmps = 100
        nchnls = 2
        0dbfs=1


        instr 1 
        kamp=p4
        kcps=p5
        aout oscili kamp,kcps
        outs aout,aout
        endin 
        ]])

        csnd.csoundReadScore(ud.csound,"i 1 0 3 0.2 200")
        csnd.csoundStart(ud.csound)
        var ThreadID=csnd.csoundCreateThread(Perf_Routine,ud)
        csnd.csoundReset(ud.csound)
end

main()
