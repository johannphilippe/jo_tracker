<CsoundSynthesizer>
<CsOptions>

-odac
</CsOptions>
<CsInstruments>
sr = 44100
ksmps = 512
nchnls = 2
0dbfs=1

gaL init 0
gaR init 0
gktempo init 0




opcode monodie,a,ppppp
	itim,iamp,ifreq,iswitch,icnt xin
	aidx linseg 0,itim,1
	inb=ftlen(iamp)

	if ftlen(ifreq)<inb then 
		inb=ftlen(ifreq) 
	endif



	if icnt>=inb goto finduvoyage
	aout monodie itim,iamp,ifreq,iswitch,icnt+1


finduvoyage:

	icur = icnt

ifno ftgentmp 0,0,-ftlen(iswitch),-2,0


isw_cnt = 0
itow = 0
until isw_cnt==ftlen(iswitch) do
	itb table isw_cnt,iswitch
	if (itb==icur) then 
		itow=1
	else 
		itow=0
	endif
	tablew itow,isw_cnt,ifno
	print isw_cnt,itow,ifno,icnt
isw_cnt=isw_cnt+1
od

	acnt init 0


	apos=acnt%ftlen(ifno)
	aval table apos+1,ifno
		
	ares lposcil 1, 1, 1, ftlen(ifno), ifno

	
	acnt=acnt+1	

	

	iampc table icnt-1,iamp
	ifq table icnt-1,ifreq

acps init 0	
if ifq>0 then 
	acps init ifq
elseif ifq<0 then
	acps tablei aidx,abs(ifq),1
endif

	print icnt,iampc,ifq
	aosc oscili iampc,acps

	aoc=aosc*ares

	amix=aout+aoc
	xout amix

endop 





opcode poly_osc,a,pppp

	itim,iamp,ifreq,icnt xin
	aidx linseg 0,itim,1
	inb=ftlen(iamp)

	if ftlen(ifreq)<inb then 
		inb=ftlen(ifreq) 
	endif



	if icnt>=inb goto finduvoyage
	aout poly_osc itim,iamp,ifreq,icnt+1


finduvoyage:

	icur = icnt



	iampc table icnt-1,iamp
	ifq table icnt-1,ifreq

acps init 0	
if ifq>0 then 
	acps init ifq
elseif ifq<0 then
	acps tablei aidx,abs(ifq),1
endif

	print icnt,iampc,ifq
	aosc oscili iampc,acps

	aoc=aosc

	amix=aout+aoc
	xout amix




endop

















/*
lua_opdef "atq", {{
	local ffi=require("ffi")
	local math=require("math")
	local string=require("string")
	local csoundApi=ffi.load("csound64")


	ffi.cdef[[
		int csoundGetKsmps(void *);
		double csoundGetSr(void *);
		struct atq_t {
			


		}

	]]

	local atq_ct=ffi.typeof('struct atq_t *')

	function atq_init(csound,opcode,carguments)
		local p=ffi.cast(atq_ct,carguments)
		p.sr=csoundApi.csoundGetSr(csound)
		p.ksmps=csoundApi.csoundGetKsmps(csound)
		if p.istor[0]==0 then
			for i=0,5 do
				.delay[i]=0.0
			end
			for i=0,3 do
				p.tanhstg[i]=0.0
			end
		end
		return 0

	end




	function atq_kontrol(csound,opcode,carguments)
		local p=ffi.cast(atq_ct,carguments)
		



	end



}}
*/
















instr 1
;info1 TEMPO
	ival = p4
	kgoto voyageisending
	outvalue "T_sync",ival
	voyageisending:
	ktempo=p3
	chnset ktempo,"tempochan"
	gktempo=p3

endin





;Synth monodique
instr 2
;info2 1_monomultiosc 2_date 3_dur 4_amptab 5_freqtab 6_switchtab 7_pan 8_amp 9_env

aidx linseg 0,abs(p3),1
iamptab init p4

ifq init p5
isw init p6

itim init abs(p3)
ao monodie itim,iamptab,ifq,isw


if p7>0 then
	apan init p7
elseif p7<0 then
	ipan init abs(p7)
	apan tablei aidx,ipan,1
endif


aenv tablei aidx,p9,1
iamp init p8
ao=ao*(aenv*iamp)

ao1,ao2 pan2 ao,apan

gaL = gaL+ao1
gaR = gaR+ao2


endin  



;Oscillateur récursif : table amp et freq
instr 3 
;info3 1_multiosc 2_amptab 3_freqtab 4_pan 5_amp 6_env


aidx linseg 0,abs(p3),1
iamptab init p4

ifq init p5

itim init abs(p3)
ao poly_osc itim,iamptab,ifq


if p6>0 then
	apan init p6
elseif p6<0 then
	ipan init abs(p6)
	apan tablei aidx,ipan,1
endif


aenv tablei aidx,p8,1
iamp init p7
ao=ao*(aenv*iamp)

ao1,ao2 pan2 ao,apan

gaL = gaL+ao1
gaR = gaR+ao2


endin



















;Oscillateur standard 

instr 5
;info5 1_oscil 2_date 3_dur 4_pitch 5_amp 6_pan 7_wave 8_env	
aidx linseg 0,abs(p3),1
kidx=k(aidx)


if p4>0 then 
	acps = 440 * exp(0.057762265 * (p4 - 69))
elseif p4<0 then
	icft init abs(p4)
	acps tablei aidx,icft,1
	acps=440 * exp(0.057762265 * (acps - 69))
endif 




if p6>=0 then
	apan = p6
elseif p6<0 then
	ipft init abs(p6)
	apan tablei aidx,ipft,1
endif 

ifn init p7
aenv tablei aidx,p8,1

aamp=p5*aenv

aout oscili aamp,acps,ifn

a1,a2 pan2 aout,apan



gaL=gaL+a1
gaR=gaR+a2



endin








instr 6
;info6 1_streson 2_date 3_dur 4_freq 5_gain 6_feedback
aenv linseg 0,p3/1000,1,p3/1000,0
anoi noise 1,0
anoi=anoi*aenv

kfreq init p4
kgain init p5
kfb init p6

aout streson anoi,kfreq,kfb
aout = aout*kgain

gaL=gaL+aout
gaR=gaR+aout



endin







instr 7
;info7 1_supermod 2_date 3_dur 4_pitch 5_amp 6_wave 7_freqmod 8_ampmod 9_vibamp 10_modwave 11_pan 12_env


	aidx linseg 0,abs(p3),1
	kidx=k(aidx)


	if p4>0 then 
		acps = 440 * exp(0.057762265 * (p4 - 69))
	elseif p4<0 then
		icft init abs(p4)
		acps tablei aidx,icft,1
		acps=440 * exp(0.057762265 * (acps - 69))
	endif 


	ifn init p6


	if p7>0 then 
		avcps = p7
	elseif p7<0 then
		ivcft init abs(p7)
		avcps tablei aidx,ivcft,1
	endif 


	if p8>=0 then
		avamp = p8
	elseif p8<0 then
		ivamp init abs(p8)
		avamp tablei aidx,ivamp,1
	endif
	ivibamp init p9
	ivibfn init p10


	avib poscil3 avamp,avcps,ivibfn
	amodamp poscil3 avamp,avcps,ivibfn


	aenv tablei aidx,p12,1

	aamp=(p5*aenv)*(amodamp+(1-avamp))

	;p5 amp / aenv env / avib vibrato / amodamp modamp / 


	tigoto nxt	
	aout oscili aamp,acps+(avib*ivibamp),ifn

	nxt:
	if p11>=0 then
		apan = p11
	elseif p11<0 then
		ipft init abs(p11)
		apan tablei aidx,ipft,1
	endif 



	a1,a2 pan2 aout,apan

	gaL=gaL+a1
	gaR=gaR+a2


	xtratim 0
	krel release
	if krel==1 kgoto rel
	kgoto suite
	rel:
	clear a1,a2


	suite:


endin 






instr 8
;info8 1_ringmod 2_date 3_dur 4_soundFile 5_amp 6_pitchoffset 7_modcps 8_modamp 9_modfn 10_pan 11_env
	aidx linseg 0,abs(p3),1

	ifile init p4
	itime=ftlen(ifile)/sr
	kfq=1/itime
	iamp init p5


	if p6>=0 then
		avib=p6
	elseif p6<0 then
		avib tablei aidx,abs(p6),1
	endif

	kcps=k(avib)+kfq
	
	if ftchnls(ifile)==1 then
	aosc loscil 1,kcps,ifile,1,1
	elseif ftchnls(ifile)==2 then
	aoscL,aoscR loscil 1,kcps,ifile,1,1
	aosc=aoscL+aoscR
	endif
	
	if p7>=0 then
		amodcps = p7
	elseif p7<0 then
		amodcps tablei aidx,abs(p7),1
	endif

	if p8>=0 then
		amodamp =p8
	elseif p8<0 then
		amodamp tablei aidx,abs(p8),1
	endif
	ifn init p9
	amod poscil3 amodamp,amodcps,ifn


	amod=(1-amodamp)+amod

	aenv tablei aidx,p11,1
	ao init 0
	ao = (aosc*amod)*(aenv*iamp)



	if p10>=0 then
		apan = p10
	elseif p10<0 then
		apan tablei aidx,abs(p10),1
	endif
	
	aoutL, aoutR pan2 ao,apan


	gaL=gaL+aoutL
	gaR=gaR+aoutR

endin





instr 9
;info9 1_record 2_date 3_dur 4_sound 5_amp 6_cps


aoutL,aoutR loscil p5,p6,p4

//aoutL aoutR flooper2 p5,p6,0.001,ftlen(p4)-0.05,0.01,p4

gaL=gaL+aoutL
gaR=gaR+aoutR

endin




instr 10









endin












;Instruments pour le synthé analogique
instr 100
;inst date dur midinote amp pan env

aidx linseg 0,abs(p3),1


iist init p1-round(p1)
imidi = iist*10
ival init p4

iamp init p5
if p6>=0 then 
	apan init p6
elseif p6<0 then
	ipf = abs(p6)
	apan tablei aidx,ipf,1
endif

ife=abs(p7)
aenv tab aidx,ife,1



if imidi==1 then
	ivel init 1
	ichan init 1
elseif imidi==2 then
	ivel init 40
	ichan init 2
elseif imidi==3 then
	ivel init 80
	ichan init 3
elseif imidi==4 then
	ivel init 120
	ichan init 4
endif


midion 1,round(ival),ivel
ain inch ichan
ain=ain*(aenv*iamp)

a1,a2 pan2 ain,apan

gaL=gaL+a1
gaR=gaR+a2

endin








instr 999
	aL=gaL
	aR=gaR
	outs aL,aR
	clear gaL,gaR
endin



</CsInstruments>
<CsScore>

t 0 60
 
f 1 0 16385 -16 0 13 0 1 16212 0 1 157 0 0 
f 2 0 16385 -16 0 26 2.29076 1 16357 -5.35326 0 
f 3 0 16385 -16 0 1638 3.87772 1 3276 -0.44837 1 4350 2.22283 0.608696 4683 -4.375 0.298913 2434 4.22283 0 
f 4 0 16385 -16 0 447 7.32775 1 6106 3.63687 0.6 1638 -1.83799 0.3 3276 1.86034 0.2 4915 -2.05773 0 
f 5 0 16385 -16 0 1210 3.67039 1 15173 -5.26629 0 
f 6 0 16385 -16 0 684 0 1 7507 3.3054 0.5 8192 -3.31471 0 
f 7 0 16385 -16 0 65 10 1 16318 -10 0 
f 8 0 16385 -16 0 8192 8.26257 0.5 3276 3.4432 1 4915 3.99255 0 
f 10 0 16385 -16 0 4724 0 0 190 0 0.8 4915 0 0.8 6553 -1.99628 0 
f 11 0 16385 -16 0 1638 0 1 6553 0 1 8192 -2.43948 0 
f 20 0 16385 -16 0 16384 0 1 
f 21 0 16385 -16 1 16384 0 0 
f 22 0 16385 -16 0 16384 8.11685 1 
f 23 0 16385 -16 1 16384 -7.33152 0 
f 24 0 16385 -16 0 16384 -6.38315 1 
f 25 0 16385 -16 1 16384 6.41848 0 
f 200 0 16385 -16 89 8192 0 89 3276 -1.97898 88 4915 -1.22368 90 
f 201 0 16385 -16 89 6553 0 89 6553 0 86 3250 0 85.7491 
f 101 0 16385 -16 11 16384 -7.99966 1 
f 302 0 0 1 "/home/johann/jo_tracker/csound/sons/cloche/PrecisDoux1.wav" 0 0 0 
f 100 0 16385 -16 1 3255 -3.9287 15.6893 3361 2.8491 4.76648 9766 -4.44146 1 
f 300 0 0 1 "/home/johann/jo_tracker/csound/sons/cloche/pasPrecisAigu1.wav" 0 0 0 
f 103 0 16385 -16 1 1001 3.70463 2000 15382 -7.12356 1 
f 900 0 16384 -9 1 1 0 
f 902 0 16384 -9 0.5 0.3 0 1 1 0 1.75 0.3 0 2.25 0.1 0 
f 301 0 0 1 "/home/johann/jo_tracker/csound/sons/cloche/PrecisBattement1.wav" 0 0 0 
f 903 0 16384 -9 1 1 0 2 0.5 0 3 0.3 0 4 0.25 0 5 0.2 0 6 0.167 0 7 0.14 0 8 0.125 0 9 0.111 0 10 0.105 0 11 0.1 0 
f 904 0 16385 -16 -1.38778e-16 4915 0 -1 914 0 -0.310987 723 0 -1 1638 0 0.1 3276 0 1 717 0 0.366853 921 0 0.992551 3276 0 -1.38778e-16 
f 905 0 16385 -16 -1.38778e-16 4132 10 -1 4059 -10 -1.38778e-16 4007 10 1 4184 -10 -1.38778e-16 
f 906 0 16384 -9 0.5 0.3 0 1 1 0 1.75 0.3 0 2.25 0.2 0 2.5 0.15 0 3.25 0.1 0 
f 104 0 16385 -16 1 3276 0 1 1638 2.53433 10 11468 -2.11343 1 
f 102 0 16385 -16 7 16384 2.95523 21 
f 901 0 16384 -9 0.5 0.5 0 1 1 0

i 1 0 1 1
i 1 1 1 2
i 1 2 1 3
i 1 3 1 4
i 1 4 1 5
i 1 5 1 6
i 1 6 1 7
i 1 7 1 8
i 1 8 1 9
i 1 9 1 10
i 1 10 1 11
i 1 11 1 12
i 1 12 1 13
i 1 13 1 14
i 1 14 1 15
i 1 15 1 16
i 1 16 1 17
i 1 17 1 18
i 1 18 1 19
 
i 8.1 0 18 301 1 1 50 1 900 0.5 1 1 
i -8.1 18 1
i 999  0 19
</CsScore>
</CsoundSynthesizer>