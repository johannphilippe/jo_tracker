
<CsoundSynthesizer>
<CsOptions>
 -o dac
</CsOptions>
<CsInstruments>
sr=44100
ksmps=32
nchnls=2
0dbfs=1

instr 1 
arnd random 100,400
ares oscili 0.2,500

outs ares,ares

endin


</CsInstruments>
<CsScore>
t 0 60 5 240 5 120 10 30 


i 1 0 1
i 1 1 1 
i 1 2 1 
i 1 3 1 
i 1 4 1
i 1 5 1
i 1 6 1
i 1 7 1
i 1 8 1
i 1 9 1
i 1 10 1
i 1 11 1 

</CsScore>
</CsoundSynthesizer>
