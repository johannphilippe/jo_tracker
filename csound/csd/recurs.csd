<CsoundSynthesizer>
<CsOptions>

-odac
</CsOptions>
<CsInstruments>
sr = 48000
ksmps = 16
nchnls = 2
0dbfs=1

gaL init 0
gaR init 0
gktempo init 0






opcode monodie,a,ppppp
	itim,iamp,ifreq,iswitch,icnt xin
	aidx linseg 0,itim,1
	inb=ftlen(iamp)

	if ftlen(ifreq)<inb then 
		inb=ftlen(ifreq) 
	endif



	if icnt>=inb goto finduvoyage
	aout monodie itim,iamp,ifreq,iswitch,icnt+1


finduvoyage:

	icur = icnt

ifno ftgentmp 0,0,-ftlen(iswitch),-2,0


isw_cnt = 0
itow = 0
until isw_cnt==ftlen(iswitch) do
	itb table isw_cnt,iswitch
	if (itb==icur) then 
		itow=1
	else 
		itow=0
	endif
	tablew itow,isw_cnt,ifno
	print isw_cnt,itow,ifno,icnt
isw_cnt=isw_cnt+1
od

	acnt init 0


	apos=acnt%ftlen(ifno)
	aval table apos+1,ifno
		
	ares lposcil 1, 1, 1, ftlen(ifno), ifno

	
	acnt=acnt+1	

	

	iampc table icnt-1,iamp
	ifq table icnt-1,ifreq

acps init 0	
if ifq>0 then 
	acps init ifq
elseif ifq<0 then
	acps tablei aidx,abs(ifq),1
endif

	print icnt,iampc,ifq
	aosc oscili iampc,acps

	aoc=aosc*ares

	amix=aout+aoc
	xout amix

endop 





opcode poly_osc,a,pppp

	itim,iamp,ifreq,icnt xin
	aidx linseg 0,itim,1
	inb=ftlen(iamp)

	if ftlen(ifreq)<inb then 
		inb=ftlen(ifreq) 
	endif



	if icnt>=inb goto finduvoyage
	aout poly_osc itim,iamp,ifreq,icnt+1


finduvoyage:

	icur = icnt



	iampc table icnt-1,iamp
	ifq table icnt-1,ifreq

acps init 0	
if ifq>0 then 
	acps init ifq
elseif ifq<0 then
	acps tablei aidx,abs(ifq),1
endif

	print icnt,iampc,ifq
	aosc oscili iampc,acps

	aoc=aosc

	amix=aout+aoc
	xout amix




endop










instr 1
	ival = p4
	kgoto voyageisending
	outvalue "T_sync",ival
	voyageisending:
	ktempo=p3
	chnset ktempo,"tempochan"
	gktempo=p3

endin





;Synth monodique
instr 2
; inst date dur tabamp,tabfreq,tabswitch,pan,amp,env
aidx linseg 0,abs(p3),1
iamptab init p4

ifq init p5
isw init p6

itim init abs(p3)
ao monodie itim,iamptab,ifq,isw


if p7>0 then
	apan init p7
elseif p7<0 then
	ipan init abs(p7)
	apan tablei aidx,ipan,1
endif


aenv tablei aidx,p9,1
iamp init p8
ao=ao*(aenv*iamp)

ao1,ao2 pan2 ao,apan

gaL = gaL+ao1
gaR = gaR+ao2


endin  




instr 3 
;p1=inst;p2=date;p3=dur;p4=env
; inst date dur tabamp,tabfreq,tabswitch,pan,amp,env
aidx linseg 0,abs(p3),1
iamptab init p4

ifq init p5

itim init abs(p3)
ao poly_osc itim,iamptab,ifq


if p6>0 then
	apan init p6
elseif p6<0 then
	ipan init abs(p6)
	apan tablei aidx,ipan,1
endif


aenv tablei aidx,p8,1
iamp init p7
ao=ao*(aenv*iamp)

ao1,ao2 pan2 ao,apan

gaL = gaL+ao1
gaR = gaR+ao2


endin




instr 4 
;def  inst date dur osc1amp osc1fq osc1tab osc2amp osc2fq osc2tab pan env
aidx linseg 0,abs(p3),1


	acnt init 0
	
	iamp1 init p4
	iamp2 init p7
	itab1 init p6
	itab2 init p9

	adx=(acnt%ftlen(itab1))
	acnt=acnt+1
	if p5>0 then 
		acps1 = 440 * exp(0.057762265 * (p5 - 69))
	elseif p5<0 then
		icft init abs(p5)
		acps1 tablei aidx,icft,1
		acps1=440 * exp(0.057762265 * (acps1 - 69))
	endif 

	if p8>0 then 
		acps2 = 440 * exp(0.057762265 * (p8 - 69))
	elseif p8<0 then
		icft init abs(p8)
		acps2 tablei aidx,icft,1
		acps2=440 * exp(0.057762265 * (acps2 - 69))
	endif 

	

	ao1 oscili iamp1,acps1
	ao2 oscili iamp2,acps2



	asel1 table adx,itab1
	asel2 table adx,itab2



	if p10>=0 then
		apan = p10
	elseif p10<0 then
		ipft init abs(p10)
		apan tablei aidx,ipft,1
	endif 

	aenv tablei aidx,p11,1

	aout=((ao1*asel1)+(ao2*asel2))*aenv
	aoL,aoR pan2 aout,apan


	gaL=gaL+aoL
	gaR=gaR+aoR



endin






instr 5
;def inst date dur pitch amp fn pan env

	
aidx linseg 0,abs(p3),1
kidx=k(aidx)


if p4>0 then 
	acps = 440 * exp(0.057762265 * (p4 - 69))
elseif p4<0 then
	icft init abs(p4)
	acps tablei aidx,icft,1
	acps=440 * exp(0.057762265 * (acps - 69))
endif 




if p6>=0 then
	apan = p6
elseif p6<0 then
	ipft init abs(p6)
	apan tablei aidx,ipft,1
endif 

ifn init p7
aenv tablei aidx,p8,1
aamp=p5*aenv

aout oscili aamp,acps,ifn

a1,a2 pan2 aout,apan
outs a1,a2

gaL=gaL+a1
gaR=gaR+a2




endin










instr 6



endin 















;Instruments pour le synthé analogique
instr 100
;inst date dur midinote amp pan env

aidx linseg 0,abs(p3),1


iist init p1-round(p1)
imidi = iist*10
ival init p4

iamp init p5
if p6>=0 then 
	apan init p6
elseif p6<0 then
	ipf = abs(p6)
	apan tablei aidx,ipf,1
endif

ife=abs(p7)
aenv tab aidx,ife,1



if imidi==1 then
	ivel init 1
	ichan init 1
elseif imidi==2 then
	ivel init 40
	ichan init 2
elseif imidi==3 then
	ivel init 80
	ichan init 3
elseif imidi==4 then
	ivel init 120
	ichan init 4
endif


midion 1,round(ival),ivel
ain inch ichan
ain=ain*(aenv*iamp)

a1,a2 pan2 ain,apan

gaL=gaL+a1
gaR=gaR+a2

endin








instr 999
	aL=gaL
	aR=gaR
	outs aL,aR
	clear gaL,gaR
endin



</CsInstruments>
<CsScore>

t 0 60
 
f 1 0 4 -2 0.5 0.4 0.3 0.2 
f 2 0 4 -2 50 -20 -21 -22 
f 3 0 8 -2 1 2 3 4 4 3 3 4 
f 10 0 2 -2 0.5 0.5 
f 11 0 2 -2 500 1000 
f 20 0 16385 -16 10000 16384 0 1700 
f 21 0 16385 -16 3000 8192 0 1000 8192 -0 9000 
f 22 0 16385 -16 200 16384 -0 300 
f 30 0 16385 -16 0 253.62229102167 10 1 16130.377708978 -10 0 
f 31 0 16385 -16 0 8192 5.1878 1 8192 7.37317 0 
f 40 0 16384 -9 1 0.5 0 2 0.5 0 0.5 0.5 0

i 1 0 1 1
i 1 1 1 2
i 1 2 1 3
i 1 3 1 4
i 1 4 1 5
i 1 5 1 6
i 1 6 1 7
i 1 7 1 8
 
i 2.1 0 6 1 2 3 -31 0 30 
i -2.1 6 1 
i 5.1 7 1 30 0.3 0.5 40 30 
i 3.1 0 6 1 2 -31 0.5 30 
i -3.1 6 1 
i 5.1 0 2 30 0.8 0.5 40 30 
i 5.1 2 2 30 0.8 0.5 40 30 
i 5.1 4 2 30 0.8 0.5 40 30 
i 5.1 6 1 30 0.8 0.5 40 30
i 999  0 8
</CsScore>
</CsoundSynthesizer>