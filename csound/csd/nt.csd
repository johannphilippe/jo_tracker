<CsoundSynthesizer>
<CsOptions>

-odac
</CsOptions>
<CsInstruments>
sr = 44100
//ksmps = 512
ksmps = 1024
nchnls = 8
0dbfs=1

gaL init 0
gaR init 0
gktempo init 0

/*
JackoInit "default","csound"
JackoInfo
JackoAudioOutConnect "output1", "REAPER:in1"
JackoAudioOutConnect "output2","REAPER:in2"
*/


opcode mtofa,a,a
	amid xin
	aout=40 * exp(0.057762265 * (amid - 69))
	xout aout
endop




opcode readtaba,a,ppp
	ifn,idur,imode xin
	aout init 0
	aidx linseg 0,abs(idur),1
	if(imode==0) then ; pan
		if(ifn>=0) then
			aout=ifn
		elseif(ifn<0) then
			aout tablei aidx,abs(ifn),1
		endif			
	elseif(imode==1) then ;env
		if(ifn>0) then
			aout tablei aidx,abs(ifn),1
		elseif (ifn<0) then
			aout linseg 0,0.001,1,abs(p3)-0.002,1,0.001,0
		endif
	elseif(imode==2) then ; cps avec conversion depuis midi
		if(ifn>0) then
			aout=mtofa(a(ifn))
		elseif (ifn<0) then
			aout= mtofa(a(tablei(aidx,abs(ifn),1)))
		endif
	elseif(imode==3) then ; cps sans conversion
		if(ifn>0) then
			aout=abs(ifn)
		elseif(ifn<0) then
			aout tablei aidx,abs(ifn),1
		endif
	endif
	xout aout
endop


opcode sfread,aa,akp
	aamp,kcps,ifn xin
	
	ich=ftchnls(ifn)
	a1 init 0
	a2 init 0	

	if(ich==1) then
		a1 loscil aamp,kcps,ifn,1
		a1=a1
	elseif (ich==2) then
		a1,a2 loscil aamp,kcps,ifn,1
	endif
	xout a1,a2

endop


/*
opcode loopread,aa,
		ifn,iamp,kstart,kend,kpitch,kfade
		iamp,kpitch,ifn,istart,iend xin

	flooper2 kamp,kpitch,kloopstart,kloopend,kfade,ifn




	xout a1,a2
endop

*/


opcode monodie,a,ppppp
	itim,iamp,ifreq,iswitch,icnt xin
	aidx linseg 0,itim,1
	inb=ftlen(iamp)

	if ftlen(ifreq)<inb then 
		inb=ftlen(ifreq) 
	endif



	if icnt>=inb goto finduvoyage
	aout monodie itim,iamp,ifreq,iswitch,icnt+1


finduvoyage:

	icur = icnt

ifno ftgentmp 0,0,-ftlen(iswitch),-2,0


isw_cnt = 0
itow = 0
until isw_cnt==ftlen(iswitch) do
	itb table isw_cnt,iswitch
	if (itb==icur) then 
		itow=1
	else 
		itow=0
	endif
	tablew itow,isw_cnt,ifno
	print isw_cnt,itow,ifno,icnt
isw_cnt=isw_cnt+1
od

	acnt init 0


	apos=acnt%ftlen(ifno)
	aval table apos+1,ifno
		
	ares lposcil 1, 1, 1, ftlen(ifno), ifno

	
	acnt=acnt+1	

	

	iampc table icnt-1,iamp
	ifq table icnt-1,ifreq

acps init 0	
if ifq>0 then 
	acps init ifq
elseif ifq<0 then
	acps tablei aidx,abs(ifq),1
endif

	print icnt,iampc,ifq
	aosc oscili iampc,acps

	aoc=aosc*ares

	amix=aout+aoc
	xout amix

endop 


opcode poly_osc,a,pppp

	itim,iamp,ifreq,icnt xin
	aidx linseg 0,itim,1
	inb=ftlen(iamp)

	if ftlen(ifreq)<inb then 
		inb=ftlen(ifreq) 
	endif



	if icnt>=inb goto finduvoyage
	aout poly_osc itim,iamp,ifreq,icnt+1


finduvoyage:

	icur = icnt



	iampc table icnt-1,iamp
	ifq table icnt-1,ifreq

acps init 0	
if ifq>0 then 
	acps init ifq
elseif ifq<0 then
	acps tablei aidx,abs(ifq),1
endif

	print icnt,iampc,ifq
	aosc oscili iampc,acps

	aoc=aosc

	amix=aout+aoc
	xout amix




endop

































instr 1
;info1 TEMPO
	ival = p4
	kgoto voyageisending
	outvalue "T_sync",ival
	voyageisending:
	ktempo=p3
	chnset ktempo,"tempochan"
	gktempo=p3

endin





;Synth monodique
instr 2
;info2 1_monomultiosc 2_date 3_dur 4_amptab 5_freqtab 6_switchtab 7_pan 8_amp 9_env

aidx linseg 0,abs(p3),1
iamptab init p4

ifq init p5
isw init p6

itim init abs(p3)
ao monodie itim,iamptab,ifq,isw


if p7>0 then
	apan init p7
elseif p7<0 then
	ipan init abs(p7)
	apan tablei aidx,ipan,1
endif


aenv tablei aidx,p9,1
iamp init p8
ao=ao*(aenv*iamp)

ao1,ao2 pan2 ao,apan

gaL = gaL+ao1
gaR = gaR+ao2


endin  



;Oscillateur récursif : table amp et freq
instr 3 
;info3 1_multiosc 2_amptab 3_freqtab 4_pan 5_amp 6_env


aidx linseg 0,abs(p3),1
iamptab init p4

ifq init p5

itim init abs(p3)
ao poly_osc itim,iamptab,ifq


if p6>0 then
	apan init p6
elseif p6<0 then
	ipan init abs(p6)
	apan tablei aidx,ipan,1
endif


aenv tablei aidx,p8,1
iamp init p7
ao=ao*(aenv*iamp)

ao1,ao2 pan2 ao,apan

gaL = gaL+ao1
gaR = gaR+ao2


endin



















;Oscillateur standard 

instr 5
;info5 1_oscil 2_date 3_dur 4_pitch 5_amp 6_pan 7_wave 8_env	
aidx linseg 0,abs(p3),1
kidx=k(aidx)


if p4>0 then 
	acps = 440 * exp(0.057762265 * (p4 - 69))
elseif p4<0 then
	icft init abs(p4)
	acps tablei aidx,icft,1
	acps=440 * exp(0.057762265 * (acps - 69))
endif 




if p6>=0 then
	apan = p6
elseif p6<0 then
	ipft init abs(p6)
	apan tablei aidx,ipft,1
endif 

ifn init p7
aenv tablei aidx,p8,1

aamp=p5*aenv

aout oscili aamp,acps,ifn

a1,a2 pan2 aout,apan



gaL=gaL+a1
gaR=gaR+a2



endin








instr 6
	;info6 1_streson 2_date 3_dur 4_freq 5_gain 6_feedback
	aenv linseg 0,p3/1000,1,p3/1000,0
	anoi noise 1,0
	anoi=anoi*aenv
	
	kfreq init p4
	kgain init p5
	kfb init p6
	
	aout streson anoi,kfreq,kfb
	aout = aout*kgain
	
	gaL=gaL+aout
	gaR=gaR+aout
endin







instr 7
;info7 1_supermod 2_date 3_dur 4_pitch 5_amp 6_wave 7_freqmod 8_ampmod 9_vibamp 10_modwave 11_pan 12_env


	aidx linseg 0,abs(p3),1
	kidx=k(aidx)


	if p4>0 then 
		acps = 440 * exp(0.057762265 * (p4 - 69))
	elseif p4<0 then
		icft init abs(p4)
		acps tablei aidx,icft,1
		acps=440 * exp(0.057762265 * (acps - 69))
	endif 


	ifn init p6


	if p7>0 then 
		avcps = p7
	elseif p7<0 then
		ivcft init abs(p7)
		avcps tablei aidx,ivcft,1
	endif 


	if p8>=0 then
		avamp = p8
	elseif p8<0 then
		ivamp init abs(p8)
		avamp tablei aidx,ivamp,1
	endif
	ivibamp init p9
	ivibfn init p10


	avib poscil3 avamp,avcps,ivibfn
	amodamp poscil3 avamp,avcps,ivibfn


	aenv tablei aidx,p12,1

	aamp=(p5*aenv)*(amodamp+(1-avamp))

	;p5 amp / aenv env / avib vibrato / amodamp modamp / 


	tigoto nxt	
	aout oscili aamp,acps+(avib*ivibamp),ifn

	nxt:
	if p11>=0 then
		apan = p11
	elseif p11<0 then
		ipft init abs(p11)
		apan tablei aidx,ipft,1
	endif 



	a1,a2 pan2 aout,apan

	gaL=gaL+a1
	gaR=gaR+a2


	xtratim 0
	krel release
	if krel==1 kgoto rel
	kgoto suite
	rel:
	clear a1,a2


	suite:


endin 






instr 8
;info8 1_ringmod 2_date 3_dur 4_soundFile 5_amp 6_pitchoffset 7_modcps 8_modamp 9_modfn 10_pan 11_env
	aidx linseg 0,abs(p3),1

	ifile init p4
	itime=ftlen(ifile)/sr
	kfq=1/itime
	iamp init p5


	if p6>=0 then
		avib=p6
	elseif p6<0 then
		avib tablei aidx,abs(p6),1
	endif

	kcps=k(avib)+kfq
	
	if ftchnls(ifile)==1 then
	aosc loscil 1,kcps,ifile,1,1
	elseif ftchnls(ifile)==2 then
	aoscL,aoscR loscil 1,kcps,ifile,1,1
	aosc=aoscL+aoscR
	endif
	
	if p7>=0 then
		amodcps = p7
	elseif p7<0 then
		amodcps tablei aidx,abs(p7),1
	endif

	if p8>=0 then
		amodamp =p8
	elseif p8<0 then
		amodamp tablei aidx,abs(p8),1
	endif
	ifn init p9
	amod poscil3 amodamp,amodcps,ifn


	amod=(1-amodamp)+amod

	aenv tablei aidx,p11,1
	ao init 0
	ao = (aosc*amod)*(aenv*iamp)



	if p10>=0 then
		apan = p10
	elseif p10<0 then
		apan tablei aidx,abs(p10),1
	endif
	
	aoutL, aoutR pan2 ao,apan


	gaL=gaL+aoutL
	gaR=gaR+aoutR

endin





instr 9
	;info9 1_record 2_date 3_dur 4_sound 5_amp 6_cps

	aoutL,aoutR loscil p5,p6,p4

	//aoutL aoutR flooper2 p5,p6,0.001,ftlen(p4)-0.05,0.01,p4
	
	gaL=gaL+aoutL
	gaR=gaR+aoutR

endin




instr 20
;info20 1_readSF 2_date 3_dur 4_sound 5_cps 6_amp 7_env
	ifn init p4
	idur init abs(p3)
	kcps init p5
	iamp init p6
	ienv init p7
	
	aamp init 0
	aamp=iamp*readtaba(ienv,idur,1)
	
	
	ao1,ao2 sfread aamp,kcps,ifn
	
	gaL=gaL+ao1
	gaR=gaR+ao2

endin







instr 98
;info98 1_midionly 2_date 3_dur 4_note 5_vel 6_chn
	inote init p4
	ivel init p5
	ichn init p6
	noteondur2 ichn,inote,ivel,abs(p3)-(abs(p3)/10)
endin


instr 99
;info99 1_midinote 2_date 3_dur 4_note 5_vel 6_chn 7_inch 8_pan 9_amp 10_env


	aidx linseg 0,abs(p3),1

	ipan init p8
	iamp init p9
	ienv init p10
	noteondur2 p6,p4,p5,abs(p3)-(abs(p3)/10)
	
	aenv tablei aidx,ienv,1
	
	if(ipan>=0) then
		apan init ipan
	elseif(ipan<0) then
		apan tablei aidx,ipan,1
	endif

	ain inch p7
	
	a1,a2 pan2 (ain*(aenv*iamp)),apan

	gaL=gaL+a1
	gaR=gaR+a2


endin




;Instruments pour le synthé analogique
instr 100
;info100 1_midi 2_date 3_dur 4_midinote 5_amp 6_pan 7_env

aidx linseg 0,abs(p3),1


iist init p1-round(p1)
imidi = iist*10
ival init p4

iamp init p5
if p6>=0 then 
	apan init p6
elseif p6<0 then
	ipf = abs(p6)
	apan tablei aidx,ipf,1
endif

ife=abs(p7)
aenv tab aidx,ife,1



if imidi==1 then
	ivel init 1
	ichan init 1
elseif imidi==2 then
	ivel init 40
	ichan init 1
elseif imidi==3 then
	ivel init 80
	ichan init 1
elseif imidi==4 then
	ivel init 120
	ichan init 1
endif


midion 1,round(ival),ivel
ain inch ichan
ain=ain*(aenv*iamp)

a1,a2 pan2 ain,apan

gaL=gaL+a1
gaR=gaR+a2

endin




instr 101
;info101 1_chnin 2_date 3_dur 4_chn 5_pan 6_amp 7_env
	tigoto noinit
	ipan init p5
	iamp init p6
	ienv init p7


	apan readtaba ipan,abs(p3),0
	aamp= iamp * readtaba(ienv,abs(p3),1) 

	ao inch p4
	a1,a2 pan2 (ao*aamp),apan
	

	gaL=gaL+a1
	gaR=gaR+a2
	noinit:

endin



;instrument de contrôle par table
instr 110
;info110 1_ctltable 2_date 3_dur 4_sig 5_amp 6_chan
	tigoto noinit
	isig init p4
	asig init 0
	kchan init p6
	iamp init p5
	asig = readtaba(isig,abs(p3),0)	

	outch kchan,asig*iamp
	noinit:
endin 


;contrôle par oscillateur
instr 111
;info111 1_ctloscil 2_date 3_dur 4_fct 5_amp 6_freq 7_env 8_chan
aidx linseg 0,abs(p3),1

	ifn init p4
	iamp init p5
	ifreq init p6
	ienv init p7
	kchan init p8

if ifreq>=0 then
	acps init ifreq
elseif ifreq<0 then
	acps tablei aidx,abs(ifreq),1
endif 


aenv tablei aidx,ienv,1
aamp=aenv*iamp


	asig poscil3 aamp,acps,ifn

outch kchan,asig



endin 



instr 112
;info112 1_sfcontrol 2_date 3_dur 4_sfile 5_cps 6_amp 7_env 8_chn
	ifn init p4
	kcps init p5
	iamp init p6
	ienv init p7
	kchn init p8
	aamp init 0
	aamp=iamp*readtaba(ienv,abs(p3),1)
	a1,a2 sfread aamp,kcps,ifn
	;kch linseg 1,abs(p3),32
	outch int(kchn),(a1+a2)

endin


/*

instr 900
;info900 1_jacktoreaper 2_date 3_dur 4_amp


	iamp init p4
	JackoAudioOut "leftReaper",gaL*iamp
	JackoAudioOut "rightReaper",gaR*iamp



endin

*/

instr 999
	aL=gaL
	aR=gaR
	outs aL,aR
	clear gaL,gaR
endin



</CsInstruments>
<CsScore>

t 0 60


i 1 0 1 1
i 1 1 1 2
i 1 2 1 3
i 1 3 1 4
i 1 4 1 5
i 1 5 1 6
i 1 6 1 7
i 1 7 1 8
i 1 8 1 1
i 1 9 1 2
i 1 10 1 3
i 1 11 1 4
i 1 12 1 5
i 1 13 1 6
i 1 14 1 7
i 1 15 1 8
i 1 16 1 1
i 1 17 1 2
i 1 18 1 3
i 1 19 1 4
i 1 20 1 5
i 1 21 1 6
i 1 22 1 7
i 1 23 1 8
i 1 24 1 1
i 1 25 1 2
i 1 26 1 3
i 1 27 1 4
i 1 28 1 5
i 1 29 1 6
i 1 30 1 7
i 1 31 1 8
i 1 32 1 1
i 1 33 1 2
i 1 34 1 3
i 1 35 1 4
i 1 36 1 5
i 1 37 1 6
i 1 38 1 7
i 1 39 1 8
i 1 40 1 1
i 1 41 1 2
i 1 42 1 3
i 1 43 1 4
i 1 44 1 5
i 1 45 1 6
i 1 46 1 7
i 1 47 1 8
i 1 48 1 1
i 1 49 1 2
i 1 50 1 3
i 1 51 1 4
i 1 52 1 5
i 1 53 1 6
i 1 54 1 7
i 1 55 1 8
 
i 5.1 0 -4 
i 5.1 4 -4 
i 5.1 8 -4 
i 5.1 12 -4 
i 5.1 16 -4 
i 5.1 20 -4 
i 5.1 24 -4 
i 5.1 28 -4 
i 5.1 32 -4 
i 5.1 36 -4 
i 5.1 40 -4 
i 5.1 44 -4 
i -5.1 48 1
i 999  0 56
</CsScore>
</CsoundSynthesizer>