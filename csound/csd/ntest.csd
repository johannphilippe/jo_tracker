<CsoundSynthesizer>
<CsOptions>

-odac
</CsOptions>
<CsInstruments>
sr = 44100
ksmps = 128
nchnls = 2
0dbfs=1

gaL init 0
gaR init 0



        instr 1
ival = p4
kgoto voyageisending
outvalue "T_sync",ival
voyageisending:
ktempo=p3
chnset ktempo,"tempochan"

endin




instr 2

kamp = p4
kcps = p5
       ares oscili kamp,kcps


gaL=gaL+ares
gaR=gaR+ares


endin  



instr 3
aenv linseg 0,p3/16,1,(p3/16)*14,1,p3/16,0
irand random 50,200

ao1 oscili 0.2,irand+50

ao2 oscili 0.2,irand+85
ao3 oscili 0.2,irand+110


gaL=gaL+((ao1+(ao2/2))*aenv)
gaR=gaR+((ao3+(ao2/2))*aenv)
endin


instr 4
event_i "i", 3,0,p3

event_i "i", 3,0,p3

event_i "i", 3,0,p3

event_i "i", 3,0,p3
endin


instr 5
	iidx linseg 0,p3,1
	iread table iidx,10,1 
	print iread
endin



instr master_instr
	aL=gaL
	aR=gaR
	outs aL,aR
	clear gaL,gaR
endin



</CsInstruments>



<CsScore>

t 0 60


i 1 0 1 1
i 1 1 1 2
i 1 2 1 3
i 1 3 1 4
i 1 4 1 5
i 1 5 1 6
i 1 6 1 7
i 1 7 1 8
 
i 4 0 2 
i 4 2 2 
i 4 4 2 
i 4 6 1 
i 4 7 1
i "master_instr" 0 8
</CsScore>
</CsoundSynthesizer>
